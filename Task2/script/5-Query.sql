WITH "all" AS (
    SELECT location_id,
           age_range,
           sum(count(*)) OVER (PARTITION BY location_id, age_range) AS "age_count",
           gender,
           sum(count(*)) OVER (PARTITION BY location_id, gender) AS "gender_count",
           officer_defined_ethnicity,
           sum(count(*)) OVER (PARTITION BY location_id, officer_defined_ethnicity) AS "ethnicity_count",
           object_of_search,
           sum(count(*)) OVER (PARTITION BY location_id, object_of_search) AS "object_count",
           outcome,
           sum(count(*)) OVER (PARTITION BY location_id, outcome) AS "outcome_count"
    FROM stop
    WHERE age_range<>''
      AND officer_defined_ethnicity<>''
      AND object_of_search<>''
      AND outcome<>''
      AND location_id IS NOT NULL
      AND to_char(datetime,'YYYY-MM') BETWEEN '2019-01' AND '2019-06'
    GROUP BY location_id, age_range, gender, officer_defined_ethnicity, object_of_search, outcome
)
SELECT DISTINCT location_id,
                st.name,
                first_value(age_range) OVER (PARTITION BY location_id ORDER BY age_count DESC) AS popular_age,
                first_value(gender) OVER (PARTITION BY location_id ORDER BY gender_count DESC) AS popular_gender,
                first_value(officer_defined_ethnicity) OVER (PARTITION BY location_id ORDER BY ethnicity_count DESC) AS popular_ethnicity,
                first_value(object_of_search) OVER (PARTITION BY location_id ORDER BY object_count DESC) AS popular_object_of_search,
                first_value(outcome) OVER (PARTITION BY location_id ORDER BY outcome_count DESC) AS popular_outcome
FROM "all"
INNER JOIN street st ON location_id = st.id
GROUP BY location_id, st.name, age_range, outcome, object_of_search, officer_defined_ethnicity, gender, age_count, gender_count, ethnicity_count, object_count, outcome_count;