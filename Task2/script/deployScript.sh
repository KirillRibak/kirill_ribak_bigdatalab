#!/bin/bash

    table1=""
    table2=""
    table3=""
    table4=""
	table5=""
    table6=""
	QUIET=0
    readonly path="/root/kirill/kirill_ribak_bigdatalab/Task2/"
    readonly drop="script/drop.sql"
    readonly create="script/table.sql"  
    readonly delete="script/deleteAllData.sql"
	
	

    function check_table(){
	table1=$(psql -U postgres crime  -c \\dt| grep -o street)
	table2=$(psql -U postgres crime  -c \\dt| grep -o outcome_status)
	table3=$(psql -U postgres crime  -c \\dt| grep -o location)
	table4=$(psql -U postgres crime  -c \\dt| grep -o crime_street)	
	table5=$(psql -U postgres crime  -c \\dt| grep -o stop)
	table6=$(psql -U postgres crime  -c \\dt| grep -o outcome_object)			
    } 

    function check_db(){
	check_table
	if [ -z $table1 ] || [ -z $table2 ] || [ -z $table3 ] || [ -z $table4 ] || [ -z $table5 ] || [ -z $table6 ]
	   then
		psql -U postgres crime -f  $path$drop
		psql -U postgres crime -f $path$create
		echo "the database was recreated."		
	   else
		echo "the database is normal"		
	   fi
    }

    function delete_all_data(){
	psql -U postgres crime -f  $path$delete
	echo "tables are clear."
    }

   
    
    function first(){
	if [ $QUIET -eq 0  ]
	then
	mvn -q -f $path clean package
	else
	mvn  -f $path clean package
	fi
	java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=first
    }
	
    function second(){
	if [ $QUIET -eq 0  ]
	then
	mvn -q -f $path clean package
	else
	mvn  -f $path clean package
	fi
	java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=second
	
    }
   
    function third(){
	if [ $QUIET -eq 0  ]
	then
	mvn -q -f $path clean package
	else
	mvn  -f $path clean package
	fi
	java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=third
    }		

    function fourth(){
	if [ $QUIET -eq 0  ]
	then
	mvn -q -f $path clean package
	else
	mvn  -f $path clean package
	fi
	java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=fourth
    }
	
	function fifth(){
	if [ $QUIET -eq 0  ]
	then
	mvn -q -f $path clean package
	else
	mvn  -f $path clean package
	fi
	java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=fifth

    }		

    function sixth(){
	if [ $QUIET -eq 0  ]
	then
	mvn -q -f $path clean package
	else
	mvn  -f $path clean package
	fi
	java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=sixth
    }
	
	function verbose(){
	case $1 in
            -1) first ;;
            -2) second ;;
			-3) third ;;
			-4) fourth;;
			-5) fifth ;;
			-6) sixth ;;
			-m) maven $2 ;; 
			-clear) delete_all_data ;;
			-check) check_db;;
             *) echo " incorrect flag for get info enter $0 help || -h || --help" ;;
                esac
	}
	
	function maven(){
		  case $1 in
            -1) java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=first ;;
            -2) java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=second ;;
			-3) java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=third ;;
			-4) java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=fourth;;
			-5) java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=fifth ;;
			-6) java -jar ${path}target/Task2-1.0-SNAPSHOT.jar -Dquery=sixth ;;
             *) echo " incorrect flag for get info enter $0 help || -h || --help" ;;
                esac
	}

function help(){
	        echo "Usage: $0 utility for uploading data from uk police api"
		echo " "
		echo " Options:                    String comand: "
		echo " -first 							 execute first query "
		echo " -second  						 execute second query"
		echo " -third  							 execute third query"
		echo " -fourth  						 execute fourth query"
		echo " -fifth  							 execute fifth query"
		echo " -sixth  						     execute sixth query"
		echo " -m                                execute without maven building."
		echo " -clear                            delete all data."
		echo " -check                            check tabe existing."
		echo " -v verbose "
		echo " "
}

if (( $# == 0 ))
 then
	echo " incorrect flag for get info enter $0 help || -h || --help"
else
	if [ "$1" = "help" ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]
	then
		help
	else	 
            case $1 in
            -first) first ;;
            -second) second ;;
			-third) third ;;
			-fourth) fourth;;
			-fifth) fifth ;;
			-sixth) sixth ;;
			-m) maven $2 $3;;
			-v) QUIET=1 ; verbose $2 $3;; 
			-clear)  delete_all_data ;;
			-check)check_db;;
             *) echo " incorrect flag for get info enter $0 help || -h || --help" ;;
                esac
	fi
 fi
