 WITH stop_and_search_by_officer_ethnicity AS (
 	SELECT stop.officer_defined_ethnicity AS officer_ethnicity,
 	COUNT(stop.id) AS stops
 	FROM stop
 	WHERE
 	to_char(stop.datetime, 'YYYY-MM')>='2019-01'
 	AND to_char(stop.datetime, 'YYYY-MM')<='2019-05'
 	GROUP BY (officer_ethnicity)
 ),
 arrest_statistic AS (
	 	SELECT stop.officer_defined_ethnicity AS officer_ethnicity,
 	COUNT(stop.id) AS stops,
	FORMAT(' %s %%', TRUNC(sum(COUNT(stop.id))over(partition by stop.officer_defined_ethnicity)/sum(COUNT(stop.id))over(), 3))
	AS arrest_statistic
 	FROM stop
 	WHERE
 	to_char(stop.datetime, 'YYYY-MM')>='2019-01'
 	AND to_char(stop.datetime, 'YYYY-MM')<='2019-05'
	and outcome='Arrest'
 	GROUP BY (officer_ethnicity)
 ),
 no_action AS (
 	SELECT stop.officer_defined_ethnicity AS officer_ethnicity,
 	COUNT(stop.id) AS stops,
	FORMAT(' %s %%', TRUNC(sum(COUNT(stop.id))over(partition by stop.officer_defined_ethnicity)/sum(COUNT(stop.id))over(), 3))
	AS no_action
 	FROM stop
 	WHERE
 	to_char(stop.datetime, 'YYYY-MM')>='2019-01'
 	AND to_char(stop.datetime, 'YYYY-MM')<='2019-05'
	and outcome='A no further action disposal'
 	GROUP BY (officer_ethnicity)
 ),
 other AS(
 	SELECT stop.officer_defined_ethnicity AS officer_ethnicity,
 	COUNT(stop.id) AS stops,
	FORMAT(' %s %%', TRUNC(sum(COUNT(stop.id))over(partition by stop.officer_defined_ethnicity)/sum(COUNT(stop.id))over(), 3))
	AS other
 	FROM stop
 	WHERE
 	to_char(stop.datetime, 'YYYY-MM')>='2019-01'
 	AND to_char(stop.datetime, 'YYYY-MM')<='2019-05'
	AND outcome!='A no further action disposal'
	AND outcome!='Arrest'
 	GROUP BY (officer_ethnicity)
 ),
 common AS(
	SELECT officer_defined_ethnicity AS eth,object_of_search,
    COUNT(object_of_search) AS value
    FROM stop
    GROUP BY eth,object_of_search,officer_defined_ethnicity
    ORDER BY eth , value DESC)
SELECT DISTINCT stop_and_search_by_officer_ethnicity.officer_ethnicity ,
stop_and_search_by_officer_ethnicity.stops,
arrest.arrest_statistic,
without_action.no_action
AS no_action_statistic,
othr.other
AS other_statistic,
FIRST_VALUE(object_of_search)OVER(PARTITION BY stop_and_search_by_officer_ethnicity.officer_ethnicity)
AS common_object
FROM stop_and_search_by_officer_ethnicity
INNER JOIN arrest_statistic arrest
ON stop_and_search_by_officer_ethnicity.officer_ethnicity = arrest.officer_ethnicity
INNER JOIN no_action without_action
ON stop_and_search_by_officer_ethnicity.officer_ethnicity = without_action.officer_ethnicity
INNER JOIN other othr
ON stop_and_search_by_officer_ethnicity.officer_ethnicity = othr.officer_ethnicity
INNER JOIN common objects
ON stop_and_search_by_officer_ethnicity.officer_ethnicity = objects.eth