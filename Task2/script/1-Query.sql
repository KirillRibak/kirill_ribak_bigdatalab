SELECT
streets.id AS street_id ,
streets.name AS street_name ,
FORMAT('from %s till %s',MIN(crime.month),MAX(crime.month)) AS "period",
COUNT(crime.id) AS crime_count
FROM public.crime_street crime
INNER JOIN location locations ON crime.location_id=locations.id
INNER JOIN street streets ON locations.street_id = streets.id
WHERE crime.month >= ? AND crime.month <= ?
GROUP BY (streets.id)
ORDER BY  (crime_count) DESC