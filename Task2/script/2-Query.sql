WITH crimes_by_category_and_month AS(
	SELECT crime_street.crime_category AS category, crime_street.month AS month,
	count(crime_street.id)AS crimes
	FROM crime_street WHERE crime_street.month >= ? AND crime_street.month <= ?
	GROUP BY (crime_street.crime_category, crime_street.month) ORDER BY category,month
)
SELECT category,month,crimes,
LAG(crimes ,1)OVER(PARTITION BY category) AS previous_month_crime ,
crimes - LAG(crimes ,1)OVER(PARTITION BY category) AS delta_count,
FORMAT(' %s %%', TRUNC((crimes - LAG(crimes ,1)OVER(PARTITION BY category))*100/LAG(crimes ,1)OVER(PARTITION BY category), 2)) AS basic_growth_rate
FROM crimes_by_category_and_month

