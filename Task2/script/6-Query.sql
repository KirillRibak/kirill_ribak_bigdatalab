  WITH crimes AS (
        SELECT location_id,
               crime_category,
               month
        FROM crime_street
        WHERE (crime_category = 'drugs'
            OR crime_category = 'possession-of-weapons'
            OR crime_category = 'theft-from-the-person'
            OR crime_category = 'shoplifting')
          AND (month between '2019-01' AND '2019-05')
          AND location_id IS NOT NULL

    ), stop_search AS (
        SELECT location_id,
               object_of_search,
               to_char(datetime, 'yyyy-MM') AS "date"
        FROM stop
        WHERE (object_of_search = 'Controlled drugs'
            OR object_of_search = 'Firearms'
            OR object_of_search = 'Offensive weapons'
            OR object_of_search = 'Stolen goods')
          AND (to_char(datetime, 'yyyy-MM') BETWEEN '2019-01' and '2019-05')
          AND outcome = 'Arrest'
          AND location_id is not null

    ), data AS (
        SELECT DISTINCT location_id,
                        month
        FROM crimes
        UNION
        DISTINCT
        SELECT location_id,
               "date"
        FROM stop_search
    )
    SELECT street.id,
           street.name,
           d.month,
           CRDrugs.drugs AS drug_crime_count,
           SSDrugs."controlled drugs" AS drug_stop_count,
           CRWeapons."possession of weapons" AS weapon_crime_count,
           SSWeapons."Offensive weapons/Firearms" AS weapon_stop_count,
           CRTheft."theft from the person/shoplifting" AS theft_crime_count ,
           SSTheft."Stolen goods" AS theft_stop_count
    FROM "data" d
             LEFT JOIN (
        SELECT location_id,
               month,
               COUNT(*) AS "drugs"
        FROM crimes
        WHERE crime_category='drugs'
        GROUP BY location_id, month
    ) AS CRDrugs ON d.location_id=CRDrugs.location_id AND d.month = CRDrugs.month
             LEFT JOIN (
        SELECT location_id,
               "date",
               COUNT(*) AS "controlled drugs"
        FROM stop_search
        WHERE object_of_search='Controlled drugs'
        GROUP BY location_id, "date"
    ) SSDrugs ON d.location_id = SSDrugs.location_id AND d.month = SSDrugs.date
             LEFT JOIN (
        SELECT location_id,
               month,
               COUNT(*) AS "possession of weapons"
        FROM crimes
        WHERE crime_category='possession-of-weapons'
        GROUP BY location_id, month
    ) CRWeapons ON d.location_id = CRWeapons.location_id AND d.month = CRWeapons.month
             LEFT JOIN (
        SELECT location_id,
               "date",
               COUNT(*) AS "Offensive weapons/Firearms"
        FROM stop_search
        WHERE object_of_search='Offensive weapons'
           OR object_of_search='Firearms'
        GROUP BY location_id, "date"
    ) SSWeapons ON d.location_id = SSWeapons.location_id AND d.month = SSWeapons.date
             LEFT JOIN (
        SELECT location_id,
               month,
               COUNT(*) AS "theft from the person/shoplifting"
        FROM crimes
        WHERE crime_category='theft-from-the-person'
           OR crime_category='shoplifting'
        GROUP BY location_id, month
    ) CRTheft ON d.location_id = CRTheft.location_id AND d.month = CRTheft.month
             LEFT JOIN (
        SELECT location_id,
               "date",
               COUNT(*) AS "Stolen goods"
        FROM stop_search
        WHERE object_of_search='Stolen goods'
        GROUP BY location_id, "date"
    ) SSTheft ON d.location_id = SSTheft.location_id AND d.month = SSTheft.date
    INNER JOIN location ON location.id = d.location_id
    INNER JOIN street ON street.id=location.street_id