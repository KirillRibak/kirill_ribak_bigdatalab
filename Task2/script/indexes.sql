CREATE INDEX crime_category_indx ON crime_street (crime_category);
CREATE INDEX outcome_status_category_indx ON outcome_status(category);
CREATE INDEX outcome_obj_name_indx ON outcome_object(name);
CREATE INDEX obj_of_search_indx ON stop(object_of_search);