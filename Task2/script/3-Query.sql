SELECT streets.id AS street_id,
streets.name AS street_name,
outcome.category AS outcome_category,
COUNT(outcome.category) AS crimes,
FORMAT(' %s %%', TRUNC(COUNT(outcome.category)*100/SUM(COUNT(outcome.category))OVER(),3)) AS percent
FROM crime_street
INNER JOIN outcome_status outcome
ON  outcome.id=crime_street.outcome_status_id
INNER JOIN location locations
ON crime_street.location_id=locations.id
INNER JOIN street streets
ON locations.street_id = streets.id
WHERE crime_street.month >= '2019-01'
AND crime_street.month <= '2019-05'
AND crime_street.outcome_status_id IS NOT NULL
AND outcome.category='Investigation complete; no suspect identified'
GROUP BY(streets.id,outcome.category)
ORDER BY (crimes) DESC