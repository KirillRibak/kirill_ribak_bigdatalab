
CREATE TABLE outcome_status
(
    id SERIAL PRIMARY KEY ,
    date varchar(10),
    category varchar(255)
);


CREATE TABLE street
(
    id serial primary key ,
    name varchar(255)
);

CREATE TABLE location
(
    id SERIAL PRIMARY KEY ,
    latitude float8  ,
    longitude float8 ,
    street_id integer ,
    foreign key (street_id) references street (id) ON DELETE SET NULL,
    unique(longitude,latitude)
);

CREATE TABLE crime_street
(
    id serial PRIMARY KEY ,
    crime_category varchar(255),
    location_type varchar(32),
    location_id integer,
    context varchar(255),
    persistence_id varchar(64),
    outcome_status_id integer,
    location_subtype varchar(255),
    month varchar(7),
    foreign key (location_id) references location (id) ON DELETE SET NULL ,
    foreign key (outcome_status_id) references outcome_status (id) ON DELETE SET NULL
);

CREATE TABLE outcome_object
(
    id varchar(60) PRIMARY KEY ,
    name varchar(255)
);

CREATE TABLE stop
(
    id serial PRIMARY KEY ,
    age_range varchar(20),
    outcome varchar(255),
    involved_person boolean,
    self_definition varchar(255),
    gender varchar(20),
    legislation varchar(255),
    outcome_linked_to_object_of_search boolean,
    datetime date,
    removal_of_more_than_outer_clothing boolean,
    outcome_object_id varchar(60),
    location_id integer,
    operation varchar(255),
    officer_defined_ethnicity varchar(20),
    type varchar(255),
    operation_name varchar(255),
    object_of_search varchar(255)
);
