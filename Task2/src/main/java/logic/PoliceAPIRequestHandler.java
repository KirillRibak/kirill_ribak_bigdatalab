package logic;

import exception.HTTPException;
import org.springframework.stereotype.Component;

@Component
public class PoliceAPIRequestHandler extends PoliceDataAPI {
    private static final String STREET_CRIME_URL =
            "https://data.police.uk/api/crimes-street/all-crime?lat=52.629729&lng=-1.131592&date=2018-01";

    private static final String FIND_FORCES = "https://data.police.uk/api/forces";

    public String getRawStreetLevelCrime() throws HTTPException {
        return getRawData(STREET_CRIME_URL);
    }

    public String getRawStreetLevelCrime(String url) throws HTTPException {
        return getRawData(url);
    }

    public String getRawDataByDate(Double latitude, Double longitude, String date) throws HTTPException {

        StringBuilder builder = new StringBuilder();
        builder.append(STREET_CRIME_URL_PATTERN);
        builder.append(LATITUDE);
        builder.append(latitude);
        builder.append(DELIMITER_WITH_LONGITUDE);
        builder.append(longitude);
        builder.append(DELIMITER_WITH_DATE);
        builder.append(date);
        return getRawData(builder.toString());
    }

    public String getRawDataByPoly(String poly, String date) throws HTTPException {
        StringBuilder builder = new StringBuilder();
        builder.append(STREET_CRIME_URL_PATTERN);
        builder.append(POLY);
        builder.append(poly);
        builder.append(DELIMITER_WITH_DATE);
        builder.append(date);
        return getRawData(builder.toString());
    }

    @Override
    public String getRawForces() throws HTTPException {
        return getRawData(FIND_FORCES);
    }

    @Override
    public String getStopsByDateAndForce(String date, String force) throws HTTPException {
        StringBuilder builder = new StringBuilder();
        builder.append(STOPS_FORCE_PATTERN);
        builder.append(force);
        builder.append(DELIMITER_WITH_DATE);
        builder.append(date);
        return getRawData(builder.toString());
    }

    @Override
    public String getStopsByDateAndLocation(String date, Double latitude, Double longitude) throws HTTPException {
        StringBuilder builder = new StringBuilder();
        builder.append(STOPS_LOCATION_PATTERN);
        builder.append(LATITUDE);
        builder.append(latitude);
        builder.append(DELIMITER_WITH_LONGITUDE);
        builder.append(longitude);
        builder.append(DELIMITER_WITH_DATE);
        builder.append(date);
        return getRawData(builder.toString());
    }
}
