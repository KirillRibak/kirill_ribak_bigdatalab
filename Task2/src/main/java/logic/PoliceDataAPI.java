package logic;

import exception.HTTPException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public abstract class PoliceDataAPI {
    protected static final String STREET_CRIME_URL_PATTERN = "https://data.police.uk/api/crimes-street/all-crime?";
    protected static final String STOPS_FORCE_PATTERN = "https://data.police.uk/api/stops-force?force=";
    protected static final String STOPS_LOCATION_PATTERN = "https://data.police.uk/api/stops-street?";
    protected static final String LATITUDE = "lat=";
    protected static final String DELIMITER_WITH_LONGITUDE = "&lng=";
    protected static final String DELIMITER_WITH_DATE = "&date=";
    protected static final String POLY = "poly=";

    protected List<String> forces = new ArrayList<>();

    abstract String getRawStreetLevelCrime(String url) throws HTTPException;

    abstract String getRawStreetLevelCrime() throws HTTPException;

    abstract String getRawDataByDate(Double latitude, Double longitude, String date) throws HTTPException;

    abstract String getRawDataByPoly(String poly, String date) throws HTTPException;

    abstract String getRawForces() throws HTTPException;

    abstract String getStopsByDateAndForce(String date, String force) throws HTTPException;

    abstract String getStopsByDateAndLocation(String date, Double latitude, Double longitude) throws HTTPException;

    protected String getRawData(String url) throws HTTPException {
        StringBuilder result = new StringBuilder();
        HttpURLConnection urlConnection = null;
        try {
            URL policeUrl = new URL(url);
            urlConnection = (HttpURLConnection) policeUrl.openConnection();
            if(checkStatus(urlConnection)) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
            }else {
                throw new HTTPException("Request returned with code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            urlConnection.disconnect();
        }
        return result.toString();
    }

    private boolean checkStatus(HttpURLConnection connection) throws IOException, HTTPException {
        int response = connection.getResponseCode();
        if (response != HttpURLConnection.HTTP_OK) {
            return false;
        }
        return true;
    }


}
