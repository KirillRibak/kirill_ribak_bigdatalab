package logic.data;

import model.dto.StopDto;
import model.entity.Location;

import java.util.List;

public interface StopDataHandler extends DataHandler<StopDto> {

    List<StopDto> getStopsByDateAndForce(String date,String Force);

    List<StopDto> getStopsByDateAndLocation(String date, Location location);
}
