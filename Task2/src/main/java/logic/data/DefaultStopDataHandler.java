package logic.data;

import exception.HTTPException;
import logic.PoliceAPIRequestHandler;
import logic.json.StopJSONMapper;
import lombok.extern.slf4j.Slf4j;
import model.dto.StopDto;
import model.entity.Location;
import org.json.JSONArray;
import org.springframework.stereotype.Component;
import service.LocationService;
import util.DataValidator;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DefaultStopDataHandler implements StopDataHandler {

    private PoliceAPIRequestHandler requestHandler;
    private StopJSONMapper mapper;
    private DataValidator validator;
    private ForceDataHandler forceDataHandler;
    private LocationService locationService;

    public DefaultStopDataHandler(PoliceAPIRequestHandler requestHandler, StopJSONMapper mapper,
                                  DataValidator validator, ForceDataHandler forceDataHandler,
                                  LocationService locationService) {
        this.requestHandler = requestHandler;
        this.mapper = mapper;
        this.validator = validator;
        this.forceDataHandler = forceDataHandler;
        this.locationService = locationService;
    }

    @Override
    public List<StopDto> fillList(List<StopDto> objects, String data) {
        JSONArray array = new JSONArray(data);
        for (int i = 0; i < array.length(); i++) {
            StopDto stopDto = mapper.convertFromJSONToObject(array.getJSONObject(i));
            objects.add(stopDto);
        }
        return objects;
    }

    @Override
    public List<StopDto> getStopsByDateAndForce(String date,String force) {
        List<StopDto> stops = new ArrayList<>();
            if (validator.checkDate(date)) {
                try {
                    String rawData = requestHandler.getStopsByDateAndForce(date, force);
                    stops.addAll(fillList(stops, rawData));
                } catch (HTTPException e) {
                    log.error(e.getMessage());
                }
            }
        return stops;
    }

    @Override
    public List<StopDto> getStopsByDateAndLocation(String date, Location location) {
        List<StopDto> stops = new ArrayList<>();
        List<Location> locations = locationService.getLocation();
        if (validator.checkDate(date)) {
            Double lat = location.getLatitude();
            Double lng = location.getLongitude();
            try {
                String rawData = requestHandler.getStopsByDateAndLocation(date, lat, lng);
                stops.addAll(fillList(stops, rawData));
            } catch (HTTPException e) {
                log.error(e.getMessage());
            }
        }
        return stops;
    }

}
