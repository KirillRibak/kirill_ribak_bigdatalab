package logic.data;

import java.util.List;

public interface ForceDataHandler extends DataHandler<String>{
    List<String> getAllForces();

}
