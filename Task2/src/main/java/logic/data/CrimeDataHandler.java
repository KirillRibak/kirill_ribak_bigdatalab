package logic.data;

import model.dto.CrimeDto;
import model.entity.Location;

import java.util.List;

public interface CrimeDataHandler extends DataHandler<CrimeDto>{
     List<CrimeDto> getAllCrimesByDefault();

     List<CrimeDto> getCrimeByDate(Location location, String date) ;

     List<CrimeDto> getCrimeByDateAndCoordinates(String date,String lat, String lng);

     List<CrimeDto> getCrimeByPoly(String date,String poly);
}
