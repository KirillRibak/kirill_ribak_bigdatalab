package logic.data;

import exception.HTTPException;
import logic.PoliceAPIRequestHandler;
import logic.json.ForceJSONMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DefaultForceDataHandler implements ForceDataHandler {

    private ForceJSONMapper forceJSONMapper;
    private PoliceAPIRequestHandler requestHandler;

    @Autowired
    public DefaultForceDataHandler(ForceJSONMapper forceJSONMapper,PoliceAPIRequestHandler requestHandler) {
        this.forceJSONMapper = forceJSONMapper;
        this.requestHandler = requestHandler;
    }

    @Override
    public List<String> getAllForces() {
        List<String> forces = new ArrayList<>();
        try {
            String rawForcesData = requestHandler.getRawForces();
            fillList(forces,rawForcesData);
        } catch (HTTPException e) {
           log.error(e.getMessage());
        }
        return forces;
    }

    @Override
    public List<String> fillList(List<String> forces, String data) {
        JSONArray jsonArray = new JSONArray(data);
        for (int i = 0; i < jsonArray.length(); i++) {
            String force = forceJSONMapper.convertFromJSONToObject(jsonArray.getJSONObject(i));
            forces.add(force);
        }
        return forces;
    }
}
