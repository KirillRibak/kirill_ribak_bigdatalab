package logic.data;

import java.util.List;

public interface DataHandler <T> {

    List<T> fillList(List<T> objects, String data);
}
