package logic.data;

import exception.DataConvertException;
import exception.HTTPException;
import logic.PoliceAPIRequestHandler;
import logic.json.CrimeJSONMapper;
import lombok.extern.slf4j.Slf4j;
import model.dto.CrimeDto;
import model.entity.Location;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import util.DataValidator;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DefaultCrimeDataHandler implements CrimeDataHandler {

    private PoliceAPIRequestHandler requestHandler;
    private CrimeJSONMapper mapper;
    private DataValidator validator;

    @Autowired
    public DefaultCrimeDataHandler(PoliceAPIRequestHandler requestHandler, CrimeJSONMapper mapper, DataValidator validator) {
        this.requestHandler = requestHandler;
        this.mapper = mapper;
        this.validator = validator;
    }

    @Override
    public List<CrimeDto> getAllCrimesByDefault() {
        List<CrimeDto> crimeDtos = new ArrayList<CrimeDto>();
        try {
            String data = requestHandler.getRawStreetLevelCrime();
            fillList(crimeDtos, data);
            return crimeDtos;
        } catch (HTTPException e) {
            log.error("Invalid request: " + e.getMessage());
        }
        return crimeDtos;
    }

    @Override
    public List<CrimeDto> getCrimeByDate(Location location, String date) {
        List<CrimeDto> crimeDtos = new ArrayList<CrimeDto>();
        try {
            if (validator.checkDate(date)) {
                String data = requestHandler.getRawDataByDate(location.getLatitude(), location.getLongitude(), date);
                fillList(crimeDtos, data);
                return crimeDtos;
            } else {
                log.error("This date -" + date + " is not valid");
            }
        } catch (HTTPException e) {
            log.error("Invalid request: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<CrimeDto> getCrimeByDateAndCoordinates(String date, String lat, String lng) {
        List<CrimeDto> crimeDtos = new ArrayList<CrimeDto>();
        try {
            if (validator.checkDate(date) && validator.checkCoordinate(lat)
                    && validator.checkCoordinate(lng)) {
                Double dLat = validator.secureTransferToDouble(lat);
                Double dLng = validator.secureTransferToDouble(lng);
                String data = requestHandler.getRawDataByDate(dLat, dLng, date);
                return fillList(crimeDtos, data);
            } else {
                log.error("Invalid coordinates: lat - " + lat + ", lng - " + lng);
            }
        } catch (DataConvertException e) {
            log.error(e.getMessage());
        } catch (HTTPException e) {
            log.error("Invalid request: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<CrimeDto> getCrimeByPoly(String date, String poly) {
        List<CrimeDto> crimeDtos = new ArrayList<CrimeDto>();
        try {
            String data = requestHandler.getRawDataByPoly(poly, date);
            return fillList(crimeDtos, data);
        } catch (HTTPException e) {
            log.error("Invalid request: " + e.getMessage());
        }
        return null;
    }


    public List<CrimeDto> fillList(List<CrimeDto> crimes, String data) {
        JSONArray jsonArray = new JSONArray(data);
        for (int i = 0; i < jsonArray.length(); i++) {
            CrimeDto crimeDto = mapper.convertFromJSONToObject(jsonArray.getJSONObject(i));
            crimes.add(crimeDto);
        }
        return crimes;
    }
}
