package logic.json;

import config.AppContext;
import model.dto.LocationDto;
import model.entity.Street;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LocationJSONMapper implements ObjectJSONMapper<LocationDto> {

    private StreetJSONMapper streetJSONMapper;

    @Autowired
    public LocationJSONMapper(StreetJSONMapper streetJSONMapper) {
        this.streetJSONMapper = streetJSONMapper;
    }

    @Override
    public LocationDto convertFromJSONToObject(JSONObject jObject) {
        if (jObject != null) {
            LocationDto location = AppContext.context.getBean(LocationDto.class);
            Double latitude = jObject.getDouble("latitude");
            Double longitude = jObject.getDouble("longitude");
            Street street = streetJSONMapper.convertFromJSONToObject(jObject.getJSONObject("street"));
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            location.setStreet(street);
            return location;
        }
        return null;
    }
}
