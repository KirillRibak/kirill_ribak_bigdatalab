package logic.json;

import config.AppContext;
import lombok.extern.slf4j.Slf4j;
import model.dto.LocationDto;
import model.dto.StopDto;
import model.entity.OutcomeObject;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import util.DateHandler;

import java.text.ParseException;
import java.util.Date;

@Component
@Slf4j
public class StopJSONMapper implements ObjectJSONMapper<StopDto> {

    private JSONObject locationJSON;
    private JSONObject outcomeObjectJSON;
    private LocationJSONMapper locationJSONMapper;
    private OutcomeObjectJSONMapper outcomeObjectJSONMapper;
    private DateHandler dateHandler;

    @Autowired
    public StopJSONMapper(LocationJSONMapper locationJSONMapper, OutcomeObjectJSONMapper outcomeObjectJSONMapper, DateHandler dateHandler) {
        this.locationJSONMapper = locationJSONMapper;
        this.outcomeObjectJSONMapper = outcomeObjectJSONMapper;
        this.dateHandler = dateHandler;
    }

    @Override
    public StopDto convertFromJSONToObject(JSONObject jObject) {
        splitToDifferentJSONObjects(jObject);
        StopDto stopDto = collectStopOptions(jObject);
        return stopDto;
    }

    private StopDto collectStopOptions(JSONObject jsonObject) {
        String ageRange = getOptionIfIsNotNull("age_range",jsonObject);
        String outcome = getOptionIfIsNotNull("outcome",jsonObject);
        Boolean involvedPerson = jsonObject.getBoolean("involved_person");
        String selfDefined = getOptionIfIsNotNull("self_defined_ethnicity", jsonObject);
        String gender = getOptionIfIsNotNull("gender",jsonObject);
        String legislation = getOptionIfIsNotNull("legislation",jsonObject);
        Boolean outcomeLinkedToObject = getOptionIfIsNotNullBoolean("outcome_linked_to_object_of_search", jsonObject);
        Date date = getDate(jsonObject);
        Boolean removalOfMoreThanOuterClothing = getOptionIfIsNotNullBoolean("removal_of_more_than_outer_clothing",
                jsonObject);
        OutcomeObject outcomeObject = outcomeObjectJSONMapper.convertFromJSONToObject(outcomeObjectJSON);
        LocationDto locationDto = locationJSONMapper.convertFromJSONToObject(locationJSON);
        Boolean operation = getOptionIfIsNotNullBoolean("operation",jsonObject);
        String officerSkin = getOptionIfIsNotNull("officer_defined_ethnicity", jsonObject);
        String type = getOptionIfIsNotNull("type",jsonObject);
        String operationName = getOptionIfIsNotNull("operation_name",jsonObject);
        String objOfSearch = getOptionIfIsNotNull("object_of_search",jsonObject);
        return collectObject(ageRange,outcome,involvedPerson,selfDefined,gender,legislation,outcomeLinkedToObject,
                date,removalOfMoreThanOuterClothing,outcomeObject,locationDto,operation,officerSkin,type,operationName,
                objOfSearch);
    }

    private StopDto collectObject(String ageRange ,String outcome,Boolean involvedPerson,String selfDefined,String gender,
                                  String legislation,Boolean outcomeLinkedToObject,Date date,
                                  Boolean removalOfMoreThanOuterClothing,OutcomeObject outcomeObject,
                                  LocationDto locationDto,Boolean operation,String officerSkin,String type,
                                  String operationName,String objOfSearch){
        StopDto stopDto = AppContext.context.getBean(StopDto.class);
        stopDto.setAgeRange(ageRange);
        stopDto.setOutcome(outcome);
        stopDto.setInvolvedPerson(involvedPerson);
        stopDto.setSelfDefinedEthnicity(selfDefined);
        stopDto.setGender(gender);
        stopDto.setLegislation(legislation);
        stopDto.setOutcomeLinkedToObjectOfSearch(outcomeLinkedToObject);
        stopDto.setDatetime(date);
        stopDto.setRemovalOfMoreThanOuterClothing(removalOfMoreThanOuterClothing);
        stopDto.setOutcomeObject(outcomeObject);
        stopDto.setLocationDto(locationDto);
        stopDto.setOperation(operation);
        stopDto.setOfficerSkin(officerSkin);
        stopDto.setType(type);
        stopDto.setOperationName(operationName);
        stopDto.setObjOfSearch(objOfSearch);
        return stopDto;
    }

    private void splitToDifferentJSONObjects(JSONObject jObject) {
        if (jObject.isNull("location")) {
            this.locationJSON = null;
        }else {
            this.locationJSON = jObject.getJSONObject("location");
        }
        if (jObject.isNull("outcome_object")) {
            this.outcomeObjectJSON = null;
        }else {
            this.outcomeObjectJSON = jObject.getJSONObject("outcome_object");
        }
    }

    private Date getDate(JSONObject jsonObject) {
        Date date = null;
        try {
            String dateString = getOptionIfIsNotNull("datetime", jsonObject);
            if(dateString!=null) {
                 date = dateHandler.convertToDate(jsonObject.getString("datetime"));
            }
        } catch (ParseException e) {
            log.error(e.getMessage());
            return null;
        }
        return date;
    }
}
