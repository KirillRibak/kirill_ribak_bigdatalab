package logic.json;

import config.AppContext;
import model.entity.OutcomeStatus;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.time.YearMonth;

@Component
public class OutcomeStatusJSONMapper implements ObjectJSONMapper<OutcomeStatus> {
    @Override
    public OutcomeStatus convertFromJSONToObject(JSONObject jObject) {
        OutcomeStatus outcomeStatus = AppContext.context.getBean(OutcomeStatus.class);
        if (jObject == null) {
            return null;
        }
        String category = getOptionIfIsNotNull("category", jObject);
        String dateString = getOptionIfIsNotNull("date",jObject);
        if (dateString!=null) {
            YearMonth date = YearMonth.parse(dateString);
            outcomeStatus.setDate(date);
        }else {
            outcomeStatus.setDate(null);
        }
        outcomeStatus.setCategory(category);
        return outcomeStatus;
    }

}
