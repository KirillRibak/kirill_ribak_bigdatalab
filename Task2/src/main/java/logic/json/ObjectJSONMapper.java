package logic.json;

import org.json.JSONObject;

public interface ObjectJSONMapper<T> {
    T convertFromJSONToObject(JSONObject jObject);

    default String getOptionIfIsNotNull(String option, JSONObject jObject) {
        if (jObject.has(option) && !jObject.isNull(option)) {
            return jObject.getString(option);
        }
        return null;
    }

    default Boolean getOptionIfIsNotNullBoolean(String option, JSONObject jObject) {
        if (jObject.has(option) && !jObject.isNull(option)) {
            return jObject.getBoolean(option);
        }
        return null;
    }
}
