package logic.json;

import config.AppContext;
import model.entity.OutcomeObject;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class OutcomeObjectJSONMapper implements ObjectJSONMapper<OutcomeObject> {
    @Override
    public OutcomeObject convertFromJSONToObject(JSONObject jObject) {
        String id = getOptionIfIsNotNull("id",jObject);
        String name = getOptionIfIsNotNull("name",jObject);
        return collectObject(id,name);
    }

    private OutcomeObject collectObject(String id, String name) {
        OutcomeObject outcomeObject = AppContext.context.getBean(OutcomeObject.class);
        outcomeObject.setId(id);
        outcomeObject.setName(name);
        return outcomeObject;
    }
}
