package logic.json;

import config.AppContext;
import model.dto.CrimeDto;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.YearMonth;

@Component
public class CrimeJSONMapper implements ObjectJSONMapper<CrimeDto> {

    private JSONObject locationJSON;
    private JSONObject outcomeStatusJSON;

    private LocationJSONMapper locationJSONMapper;
    private OutcomeStatusJSONMapper outcomeStatusJSONMapper;

    @Autowired
    public CrimeJSONMapper(LocationJSONMapper locationJSONMapper, OutcomeStatusJSONMapper outcomeStatusJSONMapper) {
        this.locationJSONMapper = locationJSONMapper;
        this.outcomeStatusJSONMapper = outcomeStatusJSONMapper;
    }

    public CrimeDto convertFromJSONToObject(JSONObject jObject) {
        splitToDifferentJSONObjects(jObject);
        CrimeDto crimeDto = collectCrimeOptions(jObject);
        return crimeDto;
    }

    public void splitToDifferentJSONObjects(JSONObject jObject) {
        this.locationJSON = jObject.getJSONObject("location");
        if (jObject.isNull("outcome_status")) {
            this.outcomeStatusJSON = null;
        } else {
            this.outcomeStatusJSON = jObject.getJSONObject("outcome_status");
        }
    }

    private CrimeDto collectCrimeOptions(JSONObject jObject) {
        String category = getOptionIfIsNotNull("category",jObject);
        String locationType = getOptionIfIsNotNull("location_type",jObject);
        String context = getOptionIfIsNotNull("context",jObject);
        String persistentId = getOptionIfIsNotNull("persistent_id",jObject);
        Integer id = jObject.getInt("id");
        String locationSubtype = getOptionIfIsNotNull("location_subtype",jObject);
        String month = getOptionIfIsNotNull("month",jObject);
        YearMonth date = YearMonth.parse(month);
        CrimeDto crimeDto = collectCrime(category, context, locationType, id, persistentId, locationSubtype, date);
        return crimeDto;
    }

    private CrimeDto collectCrime(String category, String context, String locationType, int id,
                                  String persistentId, String locationSubtype, YearMonth date) {
        CrimeDto crimeDto = AppContext.context.getBean(CrimeDto.class);
        crimeDto.setCategory(category);
        crimeDto.setContext(context);
        crimeDto.setLocationType(locationType);
        crimeDto.setId(id);
        crimeDto.setPersistentId(persistentId);
        crimeDto.setLocationSubtype(locationSubtype);
        crimeDto.setMonth(date);
        crimeDto.setLocation(locationJSONMapper.convertFromJSONToObject(locationJSON));
        crimeDto.setOutcomeStatus(outcomeStatusJSONMapper.convertFromJSONToObject(outcomeStatusJSON));
        return crimeDto;
    }

}
