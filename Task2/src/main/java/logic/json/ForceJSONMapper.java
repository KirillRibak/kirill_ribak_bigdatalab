package logic.json;

import org.json.JSONObject;

public class ForceJSONMapper implements ObjectJSONMapper<String> {

    @Override
    public String convertFromJSONToObject(JSONObject jObject) {
        String url = jObject.getString("id");
        return url;
    }

}
