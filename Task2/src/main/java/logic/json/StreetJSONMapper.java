package logic.json;

import config.AppContext;
import model.entity.Street;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class StreetJSONMapper implements ObjectJSONMapper<Street> {
    @Override
    public Street convertFromJSONToObject(JSONObject jObject) {
        Street street = AppContext.context.getBean(Street.class);
        Integer streetId = isNull(jObject);
        String name = jObject.getString("name");
        street.setId(streetId);
        street.setName(name);
        return street;
    }

    private Integer isNull(JSONObject jObject){
        if(jObject.getInt("id") == 0 ){
            return null;
        }
        return jObject.getInt("id");
    }
}
