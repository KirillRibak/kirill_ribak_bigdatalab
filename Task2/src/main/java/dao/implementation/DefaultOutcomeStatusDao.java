package dao.implementation;

import dao.OutcomeStatusDao;
import model.entity.OutcomeStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Map;

@Component
public class DefaultOutcomeStatusDao implements OutcomeStatusDao {
    private final String UPLOAD_STATUS = "INSERT INTO public.outcome_status(date,category)" +
            "VALUES(?,?)";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultOutcomeStatusDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int uploadOutcomeStatus(OutcomeStatus status) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(UPLOAD_STATUS, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, status.getDate().toString());
            ps.setString(2, status.getCategory());
            return ps;
        }, keyHolder);
        Map<String, Object> value = keyHolder.getKeyList().get(0);
        return (int) value.get("id");
    }
}
