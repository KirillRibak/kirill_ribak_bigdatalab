package dao.implementation;

import dao.LocationDao;
import dao.mapper.LocationMapper;
import lombok.extern.slf4j.Slf4j;
import model.entity.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class DefaultLocationDao implements LocationDao {

    private final String UPLOAD_LOCATION = "INSERT INTO public.location(longitude,latitude) VALUES(?,?)" +
            " ON CONFLICT (longitude,latitude) DO NOTHING; ";
    private final String LINK_LOCATION_WIT_STREET = "UPDATE public.location SET street_id = ? WHERE id=?;";
    private final String FIND_FIELD_BY_COORDINATES = "SELECT id FROM public.location WHERE longitude=? AND " +
            "latitude=?;";
    private final String GET_ALL_COORDINATES = "SELECT longitude,latitude FROM public.location ;";

    private JdbcTemplate jdbcTemplate;
    private LocationMapper locationMapper;

    @Autowired
    public DefaultLocationDao(JdbcTemplate jdbcTemplate, LocationMapper locationMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.locationMapper = locationMapper;
    }

    public List<Location> getAllCoordinates() {
        return jdbcTemplate.query(GET_ALL_COORDINATES, locationMapper);
    }

    public void linkLocationWithStreet(int locationId, int streetId) {
        jdbcTemplate.update(LINK_LOCATION_WIT_STREET, new Object[]{streetId, locationId});
    }

    public int uploadLocation(Location location) {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection
                        .prepareStatement(UPLOAD_LOCATION, Statement.RETURN_GENERATED_KEYS);
                ps.setDouble(1, location.getLongitude());
                ps.setDouble(2, location.getLatitude());
                return ps;
            }, keyHolder);
            Map<String, Object> value = keyHolder.getKeyList().get(0);
            return (int) value.get("id");
        } catch (IndexOutOfBoundsException e) {
            return getLocationIdByLatitudeAndLongitude(location.getLatitude(), location.getLongitude());
        }

    }

    public int uploadLocation(Double latitude, Double longitude) {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection
                        .prepareStatement(UPLOAD_LOCATION, Statement.RETURN_GENERATED_KEYS);
                ps.setDouble(1, longitude);
                ps.setDouble(2,latitude);
                return ps;
            }, keyHolder);
            Map<String, Object> value = keyHolder.getKeyList().get(0);
            return (int) value.get("id");
        } catch (DuplicateKeyException e) {
            return getLocationIdByLatitudeAndLongitude(latitude, longitude);
        }

    }

    public int getLocationIdByLatitudeAndLongitude(Double latitude, Double longitude) {
        Integer locationId = null;
        try {
             locationId= jdbcTemplate.queryForObject(FIND_FIELD_BY_COORDINATES,
                    new Object[]{longitude, latitude}, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            locationId = uploadLocation(latitude,longitude);
        }
        return locationId;
    }

}
