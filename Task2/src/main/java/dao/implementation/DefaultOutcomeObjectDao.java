package dao.implementation;

import dao.OutcomeObjectDao;
import lombok.extern.slf4j.Slf4j;
import model.entity.OutcomeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DefaultOutcomeObjectDao implements OutcomeObjectDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultOutcomeObjectDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String UPLOAD_OUTCOME_OBJECT = "INSERT INTO " +
            "public.outcome_object(id,name)" +
            "VALUES(?,?) ON CONFLICT (id)" +
            "DO NOTHING;";

    @Override
    public void uploadOutcomeObjectToDb(OutcomeObject outcomeObject) {
        jdbcTemplate.update(UPLOAD_OUTCOME_OBJECT,new Object[]{outcomeObject.getId(),outcomeObject.getName()});
    }
}
