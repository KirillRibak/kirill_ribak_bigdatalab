package dao.implementation;

import dao.CrimeDao;
import lombok.extern.slf4j.Slf4j;
import model.entity.Crime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DefaultCrimeDao implements CrimeDao {
    private JdbcTemplate jdbcTemplate;

    private final String UPLOAD_CRIME = "INSERT INTO public.crime_street(id,crime_category,location_type,location_id,context," +
            "persistence_id,outcome_status_id,location_subtype,month) " +
            "VALUES(?,?,?,?,?,?,?,?,?)  ON CONFLICT (id) DO NOTHING;";
    private final String UPLOAD_CRIME_WITHOUT_STATUS = "INSERT INTO public.crime_street(id,crime_category,location_type,location_id,context," +
            "persistence_id,location_subtype,month) " +
            "VALUES(?,?,?,?,?,?,?,?) ON CONFLICT (id) DO NOTHING;";

    @Autowired
    public DefaultCrimeDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void uploadCrimeToDb(Crime crime) {
            if (crime.getOutcomeStatusId() == null) {
                uploadCrimeWithoutStatus(crime);
            } else {
                uploadCrime(crime);
            }
    }

    private void uploadCrime(Crime crime) {
        try {
            Object[] options = {crime.getId(),
                    crime.getCrimeCategory(),
                    crime.getLocationType(),
                    crime.getLocationId(),
                    crime.getContext(),
                    crime.getPersistentId(),
                    crime.getOutcomeStatusId(),
                    crime.getLocationSubtype(),
                    crime.getMonth()};
            jdbcTemplate.update(UPLOAD_CRIME, options);
        } catch (DuplicateKeyException e) {
            log.error(e.getMessage());
        }
    }

    private void uploadCrimeWithoutStatus(Crime crime) {
        try {
            Object[] options = {crime.getId(),
                    crime.getCrimeCategory(),
                    crime.getLocationType(),
                    crime.getLocationId(),
                    crime.getContext(),
                    crime.getPersistentId(),
                    crime.getLocationSubtype(),
                    crime.getMonth()};
            jdbcTemplate.update(UPLOAD_CRIME_WITHOUT_STATUS, options);
        } catch (DuplicateKeyException e) {
            log.error(e.getMessage());
        }
    }

}