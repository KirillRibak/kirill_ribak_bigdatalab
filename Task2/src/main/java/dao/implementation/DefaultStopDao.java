package dao.implementation;

import dao.StopDao;
import lombok.extern.slf4j.Slf4j;
import model.entity.Stop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DefaultStopDao implements StopDao {
    private JdbcTemplate jdbcTemplate;

    private static final String UPLOAD_STOP = "INSERT INTO public.stop (age_range, outcome, involved_person," +
            "self_definition, gender, legislation, outcome_linked_to_object_of_search," +
            "datetime, removal_of_more_than_outer_clothing, outcome_object_id," +
            " location_id, operation, officer_defined_ethnicity, type, operation_name, object_of_search)" +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    @Autowired
    public DefaultStopDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void uploadStopToDb(Stop stop) {
        log.info("uploading stop and search json");
        Object[] params = new Object[]{
                stop.getAgeRange(),
                stop.getOutcome(),
                stop.getInvolvedPerson(),
                stop.getSelfDefinedEthnicity(),
                stop.getGender(),
                stop.getLegislation(),
                stop.getOutcomeLinkedToObjectOfSearch(),
                stop.getDatetime(),
                stop.getRemovalOfMoreThanOuterClothing(),
                stop.getOutcomeObject(),
                stop.getLocationId(),
                stop.getOperation(),
                stop.getOfficerSkin(),
                stop.getType(),
                stop.getOperationName(),
                stop.getObjOfSearch()
        };
        jdbcTemplate.update(UPLOAD_STOP,params);
    }
}
