package dao.implementation;

import dao.StreetDao;
import model.entity.Street;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Map;

@Component
public class DefaultStreetDao implements StreetDao {

    private final String UPLOAD_STREET = "INSERT INTO public.street(id,name) VALUES (?,?) ON CONFLICT (id) DO NOTHING;";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultStreetDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void uploadStreet(final Street street) {
        jdbcTemplate.update(UPLOAD_STREET,new Object[]{street.getId(), street.getName()});
    }

}
