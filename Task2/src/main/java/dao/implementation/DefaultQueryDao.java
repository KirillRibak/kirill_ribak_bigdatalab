package dao.implementation;

import dao.QueryDao;
import dao.mapper.*;
import model.entity.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultQueryDao implements QueryDao {

    private JdbcTemplate jdbcTemplate;
    private FirstQueryMapper firstQueryMapper;
    private SecondQueryMapper secondQueryMapper;
    private ThirdQueryMapper thirdQueryMapper;
    private FourthQueryMapper fourthQueryMapper;
    private FifthQueryMapper fifthQueryMapper;
    private SixthQueryMapper sixthQueryMapper;

    @Autowired
    public DefaultQueryDao(JdbcTemplate jdbcTemplate, FirstQueryMapper firstQueryMapper,
                           SecondQueryMapper secondQueryMapper, ThirdQueryMapper thirdQueryMapper,
                           FourthQueryMapper fourthQueryMapper, FifthQueryMapper fifthQueryMapper,
                           SixthQueryMapper sixthQueryMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.firstQueryMapper = firstQueryMapper;
        this.secondQueryMapper = secondQueryMapper;
        this.thirdQueryMapper = thirdQueryMapper;
        this.fourthQueryMapper = fourthQueryMapper;
        this.fifthQueryMapper = fifthQueryMapper;
        this.sixthQueryMapper = sixthQueryMapper;
    }

    @Override
    public List<FirstQuery> executeFirstQuery(String startDate, String finishDate) {
        return jdbcTemplate.query(Query.FIRST_QUERY, new Object[]{startDate, finishDate}, firstQueryMapper);
    }

    @Override
    public List<SecondQuery> executeSecondQuery(String startDate, String finishDate) {
        return jdbcTemplate.query(Query.SECOND_QUERY, new Object[]{startDate, finishDate}, secondQueryMapper);
    }

    @Override
    public List<ThirdQuery> executeThirdQuery(String startDate, String finishDate,String category) {
        return jdbcTemplate.query(Query.THIRD_QUERY, new Object[]{startDate, finishDate,category}, thirdQueryMapper);
    }

    @Override
    public List<FourthQuery> executeFourthQuery(String startDate, String finishDate) {
        return jdbcTemplate.query(Query.FOURTH_QUERY, new Object[]{
                startDate, finishDate,
                startDate, finishDate,
                startDate, finishDate,
                startDate, finishDate}, fourthQueryMapper);
    }

    @Override
    public List<FifthQuery> executeFifthQuery(String startDate, String finishDate) {
        return jdbcTemplate.query(Query.FIFTH_QUERY, new Object[]{startDate, finishDate,}, fifthQueryMapper);
    }

    @Override
    public List<SixthQuery> executeSixthQuery(String startDate, String finishDate) {

        return jdbcTemplate.query(Query.SIXTH_QUERY,new Object[]{
                startDate, finishDate,
                startDate, finishDate},sixthQueryMapper);
    }

}
