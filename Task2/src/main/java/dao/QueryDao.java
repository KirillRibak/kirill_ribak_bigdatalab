package dao;

import model.entity.query.*;

import java.util.List;

public interface QueryDao {

    List<FirstQuery> executeFirstQuery(String startDate, String finishDate);

    List<SecondQuery> executeSecondQuery(String startDate, String finishDate);

    List<ThirdQuery> executeThirdQuery(String startDate, String finishDate , String category);

    List<FourthQuery> executeFourthQuery(String startDate, String finishDate);

    List<FifthQuery> executeFifthQuery(String startDate, String finishDate);

    List<SixthQuery> executeSixthQuery(String startDate, String finishDate);
}
