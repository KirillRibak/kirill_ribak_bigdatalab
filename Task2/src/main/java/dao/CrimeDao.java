package dao;

import model.entity.Crime;

public interface CrimeDao {
    void uploadCrimeToDb(Crime crime);
}
