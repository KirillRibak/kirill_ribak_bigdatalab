package dao;

import model.entity.Location;

import java.util.List;

public interface LocationDao {
    List<Location> getAllCoordinates();

    void linkLocationWithStreet(int locationId, int streetId);

    int uploadLocation(Location location);

    int getLocationIdByLatitudeAndLongitude(Double latitude, Double longitude);

    int uploadLocation(Double latitude, Double longitude);

}
