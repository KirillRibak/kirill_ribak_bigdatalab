package dao;

import model.entity.Street;

public interface StreetDao {
    void uploadStreet(Street street);
}
