package dao.mapper;

import config.AppContext;
import model.entity.query.FifthQuery;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FifthQueryMapper implements RowMapper <FifthQuery> {
    @Override
    public FifthQuery mapRow(ResultSet resultSet, int i) throws SQLException {
        FifthQuery fifthQuery = AppContext.context.getBean(FifthQuery.class);
        fifthQuery.setAgeRange(resultSet.getString("popular_age"));
        fifthQuery.setEthnicity(resultSet.getString("popular_ethnicity"));
        fifthQuery.setGender(resultSet.getString("popular_gender"));
        fifthQuery.setObjectOfSearch(resultSet.getString("popular_object_of_search"));
        fifthQuery.setOutcome(resultSet.getString("popular_outcome"));
        return fifthQuery;
    }
}
