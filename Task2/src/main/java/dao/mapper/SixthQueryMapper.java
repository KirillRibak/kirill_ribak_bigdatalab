package dao.mapper;

import config.AppContext;
import model.entity.query.SixthQuery;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SixthQueryMapper implements RowMapper<SixthQuery> {
    @Override
    public SixthQuery mapRow(ResultSet resultSet, int i) throws SQLException {
        SixthQuery sixthQuery = AppContext.context.getBean(SixthQuery.class);
        sixthQuery.setId(resultSet.getInt("id"));
        sixthQuery.setName(resultSet.getString("name"));
        sixthQuery.setMonth(resultSet.getString("month"));
        sixthQuery.setCrimeDrug(resultSet.getInt("drug_crime_count"));
        sixthQuery.setCrimeTheft(resultSet.getInt("theft_crime_count"));
        sixthQuery.setCrimeWeapon(resultSet.getInt("weapon_crime_count"));
        sixthQuery.setStopDrug(resultSet.getInt("drug_stop_count"));
        sixthQuery.setStopTheft(resultSet.getInt("theft_stop_count"));
        sixthQuery.setStopWeapon(resultSet.getInt("weapon_stop_count"));
        return sixthQuery;
    }
}
