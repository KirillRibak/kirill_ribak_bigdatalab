package dao.mapper;

import config.AppContext;
import model.entity.query.FirstQuery;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FirstQueryMapper implements RowMapper<FirstQuery> {
    @Override
    public FirstQuery mapRow(ResultSet resultSet, int i) throws SQLException {
        FirstQuery firstQuery = AppContext.context.getBean(FirstQuery.class);
        firstQuery.setStreetId(resultSet.getInt("street_id"));
        firstQuery.setStreetName(resultSet.getString("street_name"));
        firstQuery.setPeriod(resultSet.getString("period"));
        firstQuery.setCrimeCount(resultSet.getInt("crime_count"));
        return firstQuery;
    }
}
