package dao.mapper;

import config.AppContext;
import model.entity.query.SecondQuery;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.YearMonth;

@Component
public class SecondQueryMapper implements RowMapper<SecondQuery> {

    private static final String EMPTY_PERCENT = "%";
    private static final String ARE_NOT_PERCENT = "There are no statistics for this month for this category.";

    @Override
    public SecondQuery mapRow(ResultSet resultSet, int i) throws SQLException {
        SecondQuery secondQuery = AppContext.context.getBean(SecondQuery.class);
        secondQuery.setCategory(resultSet.getString("category"));
        secondQuery.setMonth(YearMonth.parse(resultSet.getString("month")));
        secondQuery.setCrimes(resultSet.getInt("crimes"));
        secondQuery.setPreviousMonthCrimes(resultSet.getInt("previous_month_crime"));
        secondQuery.setDeltaCount(resultSet.getInt("delta_count"));
        String percent = resultSet.getString("basic_growth_rate").trim();
        if (EMPTY_PERCENT.equals(percent)){
            secondQuery.setPercent(ARE_NOT_PERCENT);
        }else {
            secondQuery.setPercent(percent);
        }
        return secondQuery;
    }
}
