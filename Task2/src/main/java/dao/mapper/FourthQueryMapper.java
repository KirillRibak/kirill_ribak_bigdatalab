package dao.mapper;

import config.AppContext;
import model.entity.query.FourthQuery;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class FourthQueryMapper implements RowMapper<FourthQuery> {
    @Override
    public FourthQuery mapRow(ResultSet resultSet, int i) throws SQLException {
        FourthQuery fourthQuery = AppContext.context.getBean(FourthQuery.class);
        fourthQuery.setArrestStatistic(resultSet.getString("arrest_statistic"));
        fourthQuery.setStopAndSearchCount(resultSet.getInt("stops"));
        fourthQuery.setMostCommonObjectOfSearch(resultSet.getString("common_object"));
        fourthQuery.setNoActionStatistic(resultSet.getString("no_action_statistic"));
        fourthQuery.setOfficerEthnicity(resultSet.getString("officer_ethnicity"));
        fourthQuery.setOtherStatistic(resultSet.getString("other_statistic"));
        return fourthQuery;
    }
}
