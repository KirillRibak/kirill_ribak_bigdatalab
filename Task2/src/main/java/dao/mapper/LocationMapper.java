package dao.mapper;

import model.entity.Location;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class LocationMapper implements RowMapper<Location> {
    public Location mapRow(ResultSet resultSet, int i) throws SQLException {
        Location location = new Location();
        location.setLongitude(resultSet.getDouble("longitude"));
        location.setLatitude(resultSet.getDouble("latitude"));
        return location;
    }
}
