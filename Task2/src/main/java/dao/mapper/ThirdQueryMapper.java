package dao.mapper;

import config.AppContext;
import model.entity.query.ThirdQuery;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ThirdQueryMapper implements RowMapper<ThirdQuery> {
    @Override
    public ThirdQuery mapRow(ResultSet resultSet, int i) throws SQLException {
        ThirdQuery thirdQuery = AppContext.context.getBean(ThirdQuery.class);
        thirdQuery.setStreetId(resultSet.getInt("street_id"));
        thirdQuery.setCountCrimes(resultSet.getInt("crimes"));
        thirdQuery.setStreetName(resultSet.getString("street_name"));
        thirdQuery.setOutcomeCategory(resultSet.getString("outcome_category"));
        thirdQuery.setPercent(resultSet.getString("percent"));
        return thirdQuery;
    }
}
