package dao;

import model.entity.OutcomeObject;

public interface OutcomeObjectDao {
    void uploadOutcomeObjectToDb(OutcomeObject outcomeObject);
}
