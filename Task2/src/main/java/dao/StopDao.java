package dao;

import model.entity.Stop;

public interface StopDao {
    void uploadStopToDb(Stop stop);
}
