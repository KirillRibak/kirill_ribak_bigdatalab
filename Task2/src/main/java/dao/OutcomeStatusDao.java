package dao;

import model.entity.OutcomeStatus;

public interface OutcomeStatusDao {
    int uploadOutcomeStatus(OutcomeStatus status);
}
