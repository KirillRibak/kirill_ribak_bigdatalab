package config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import controller.Controller;
import controller.Switcher;
import dao.*;
import dao.implementation.*;
import dao.mapper.*;
import logic.PoliceAPIRequestHandler;
import logic.data.*;
import logic.json.*;
import model.dto.CrimeDto;
import model.dto.LocationConverter;
import model.dto.LocationDto;
import model.dto.StopDto;
import model.dto.converter.CrimeConverter;
import model.dto.converter.StopConverter;
import model.entity.*;
import model.entity.query.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import service.*;
import service.implementation.*;
import util.DataValidator;
import util.DateHandler;
import util.FileHandler;

@Configuration
@PropertySource("classpath:datasource.properties")
public class ApplicationConfig {

    @Value("${jdbcUrl}")
    private String url;
    @Value("${dataSource.user}")
    private String user;
    @Value("${driver}")
    private String driver;
    @Value("${dataSource.password}")
    private String password;
    @Value("${max_size}")
    private int maxSize;

    @Bean
    public HikariConfig getHikariConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setUsername(user);
        config.setPassword(password);
        config.setDriverClassName(driver);
        config.setMaximumPoolSize(maxSize);
        return config;
    }

    @Bean
    public HikariDataSource dataSource() {
        return new HikariDataSource(getHikariConfig());
    }


    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    @Scope("prototype")
    public CrimeDto getCrimeDto() {
        return new CrimeDto();
    }

    @Bean
    @Scope("prototype")
    public LocationDto getLocationDto() {
        return new LocationDto();
    }

    @Bean
    @Scope("prototype")
    public Street getStreet() {
        return new Street();
    }

    @Bean
    @Scope("prototype")
    public Crime getCrime() {
        return new Crime();
    }

    @Bean
    @Scope("prototype")
    public Location getLocation() {
        return new Location();
    }

    @Bean
    @Scope("prototype")
    public OutcomeStatus getOutcomeStatus() {
        return new OutcomeStatus();
    }

    @Bean
    @Scope("prototype")
    public StopDto getStopDto() {
        return new StopDto();
    }

    @Bean
    @Scope("prototype")
    public Stop getStop() {
        return new Stop();
    }

    @Bean
    @Scope("prototype")
    public OutcomeObject getOutcomeObject() {
        return new OutcomeObject();
    }

    @Bean
    @Scope("prototype")
    public FirstQuery getFirstQuery() {
        return new FirstQuery();
    }

    @Bean
    @Scope("prototype")
    public SecondQuery getSecondQuery() {
        return new SecondQuery();
    }

    @Bean
    @Scope("prototype")
    public ThirdQuery getThirdQuery() {
        return new ThirdQuery();
    }

    @Bean
    @Scope("prototype")
    public FourthQuery getFourthQuery() {
        return new FourthQuery();
    }

    @Bean
    @Scope("prototype")
    public FifthQuery getFifthQuery() {
        return new FifthQuery();
    }

    @Bean
    @Scope("prototype")
    public SixthQuery getSixthQuery() {
        return new SixthQuery();
    }

    @Bean
    public PoliceAPIRequestHandler getRequestHandler() {
        return new PoliceAPIRequestHandler();
    }

    @Bean
    public CrimeJSONMapper getObjectMaper() {
        return new CrimeJSONMapper(getLocationJSONMapper(), getOutcomeStatusJSONMapper());
    }

    @Bean
    public CrimeDataHandler getDataHandler() {
        return new DefaultCrimeDataHandler(getRequestHandler(), getObjectMaper(), getDataChecker());
    }

    @Bean
    public CrimeDao getCrimeDao() {
        return new DefaultCrimeDao(jdbcTemplate());
    }

    @Bean
    public LocationMapper getLocationMapper() {
        return new LocationMapper();
    }

    @Bean
    public LocationDao getLocationDao() {
        return new DefaultLocationDao(jdbcTemplate(), getLocationMapper());
    }

    @Bean
    public FileHandler getExcelHandler() {
        return new FileHandler();
    }

    @Bean
    public LocationConverter getLocationConverter() {
        return new LocationConverter();
    }

    @Bean
    public CrimeConverter getCrimeConverter() {
        return new CrimeConverter();
    }

    @Bean
    public StreetDao getStreetDao() {
        return new DefaultStreetDao(jdbcTemplate());
    }

    @Bean
    public OutcomeStatusDao getOutcomrStatusDao() {
        return new DefaultOutcomeStatusDao(jdbcTemplate());
    }

    @Bean
    public StreetService getStreetService() {
        return new DefaultStreetService(getStreetDao());
    }

    @Bean
    public LocationService getLocationService() {
        return new DefaultLocationService(getFileHandler(), getLocationConverter(), getLocationDao());
    }

    @Bean
    public CrimeService getDataService() {
        return new DefaultCrimeService(getCrimeConverter(), getCrimeDao(),
                getOutcomrStatusDao(), getFileHandler(), getStreetService(),
                getLocationService(), getDataHandler());
    }

    @Bean
    public FileHandler getFileHandler() {
        return new FileHandler();
    }

    @Bean
    public DataValidator getDataChecker() {
        return new DataValidator();
    }

    @Bean
    public DateHandler getDateHandler() {
        return new DateHandler(getDataChecker());
    }

    @Bean
    public Controller getInterface() {
        return new Controller(getQueryService(), getSwitcher(),getFileHandler());
    }

    @Bean
    public ForceJSONMapper getForceJSONMapper() {
        return new ForceJSONMapper();
    }

    @Bean
    public ForceDataHandler getForceDataHandler() {
        return new DefaultForceDataHandler(getForceJSONMapper(), getRequestHandler());
    }

    @Bean
    public OutcomeObjectDao getOutcomeObjectDao() {
        return new DefaultOutcomeObjectDao(jdbcTemplate());
    }

    @Bean
    public StopDao getStopDao() {
        return new DefaultStopDao(jdbcTemplate());
    }

    @Bean
    public StopDataHandler getStopDataHandler() {
        return new DefaultStopDataHandler(getRequestHandler(), getStopJSONMapper(), getDataChecker(), getForceDataHandler(),
                getLocationService());
    }

    @Bean
    public StopJSONMapper getStopJSONMapper() {
        return new StopJSONMapper(getLocationJSONMapper(), getOutcomeObjectJSONMapper(), getDateHandler());
    }

    @Bean
    public StreetJSONMapper getStreetJSONMApper() {
        return new StreetJSONMapper();
    }

    @Bean
    public OutcomeObjectJSONMapper getOutcomeObjectJSONMapper() {
        return new OutcomeObjectJSONMapper();
    }

    @Bean
    public LocationJSONMapper getLocationJSONMapper() {
        return new LocationJSONMapper(getStreetJSONMApper());
    }

    @Bean
    public OutcomeStatusJSONMapper getOutcomeStatusJSONMapper() {
        return new OutcomeStatusJSONMapper();
    }

    @Bean
    public StopConverter getStopConverter() {
        return new StopConverter();
    }

    @Bean
    public OutcomeObjectService getOutcomeObjectService() {
        return new DefaultOutcomeObjectService(getOutcomeObjectDao());
    }

    @Bean
    public StopService getStopService() {
        return new DefaultStopService(getStopDao(), getStopConverter(), getLocationService(),
                getStreetService(), getOutcomeObjectService(),
                getStopDataHandler(), getForceDataHandler());
    }

    @Bean
    public FirstQueryMapper getQueryMapper() {
        return new FirstQueryMapper();
    }

    @Bean
    public QueryDao getQueryDao() {
        return new DefaultQueryDao(jdbcTemplate(), getQueryMapper(), getSecondQueryMapper(),
                getThirdQueryMapper(), getFourthQueryMapper(), getFifthQueryMapper(),getSixthQueryMapper());
    }

    @Bean
    public QueryService getQueryService() {
        return new DefaultQueryService(getQueryDao(), getDataChecker());
    }

    @Bean
    public Switcher getSwitcher() {
        return new Switcher(getDataHandler(), getDataService(), getDateHandler(), getStopService());
    }

    @Bean
    public SecondQueryMapper getSecondQueryMapper() {
        return new SecondQueryMapper();
    }

    @Bean
    public ThirdQueryMapper getThirdQueryMapper() {
        return new ThirdQueryMapper();
    }

    @Bean
    public FourthQueryMapper getFourthQueryMapper() {
        return new FourthQueryMapper();
    }

    @Bean
    public FifthQueryMapper getFifthQueryMapper() {
        return new FifthQueryMapper();
    }

    @Bean
    public SixthQueryMapper getSixthQueryMapper(){
        return new SixthQueryMapper();
    }
}
