package controller;

import config.AppContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
@Slf4j
public class Main {

    public static void main(String[] args) throws Exception {
        Controller appInterface = AppContext.context.getBean(Controller.class);
        Options options = new Options();
        Option propertyOption = Option.builder()
                .longOpt("D")
                .argName("property=value")
                .hasArgs()
                .valueSeparator()
                .numberOfArgs(2)
                .desc("use value for given properties")
                .build();
        options.addOption(propertyOption);
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        if (cmd.hasOption("D")) {
            Properties properties = cmd.getOptionProperties("D");
            String query = properties.getProperty("query");
            String start = properties.getProperty("start");
            String end = properties.getProperty("end");
            appInterface.handleRequest(start, end,query);
            log.debug("options was send");
        }
    }
}
