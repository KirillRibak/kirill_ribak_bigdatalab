package controller;

import logic.data.CrimeDataHandler;
import logic.data.StopDataHandler;
import lombok.extern.slf4j.Slf4j;
import model.dto.CrimeDto;
import model.dto.StopDto;
import model.entity.Location;
import model.entity.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.CrimeService;
import service.LocationService;
import service.QueryService;
import service.StopService;
import util.DateHandler;
import util.FileHandler;

import java.util.List;

@Component
@Slf4j
public class Controller {

    private static final String FIRST = "first";
    private static final String SECOND = "second";
    private static final String THIRD = "third";
    private static final String FOURTH = "fourth";
    private static final String FIFTH = "fifth";
    private static final String SIXTH = "sixth";
    private static final String RANGE = "range";
    private static final String QUERY = "query";

    private static final String START = "2019-01";
    private static final String FINISH = "2019-05";
    private static final String CATEGORY = "Investigation complete; no suspect identified";

    private QueryService queryService;
    private Switcher switcher;
    private FileHandler fileHandler;

    @Autowired
    public Controller(QueryService queryService, Switcher switcher, FileHandler fileHandler) {
        this.queryService = queryService;
        this.switcher = switcher;
        this.fileHandler = fileHandler;
    }

    public void handleRequest(String start, String end , String query) {
        String theChoice = switcher.getDecision(start,end,query);
        switch (theChoice){
            case QUERY: {
                switch (query) {
                    case FIRST: {
                        List<FirstQuery> firstQueries = queryService.executeFirstQuery(START, FINISH);
                        for(int i=0;i<20;i++){
                            System.out.println(firstQueries.get(i).toString());
                        }
                        fileHandler.writeFirst(firstQueries);
                        break;
                    }
                    case SECOND: {
                        List<SecondQuery> secondQueries = queryService.executeSecondQuery(START, FINISH);
                        for(int i=0;i<20;i++){
                            System.out.println(secondQueries.get(i).toString());
                        }
                        fileHandler.writeSecond(secondQueries);
                        break;
                    }
                    case THIRD: {
                        List<ThirdQuery> thirdQueries = queryService.executeThirdQuery(START,FINISH,CATEGORY);
                        for(int i=0;i<20;i++){
                            System.out.println(thirdQueries.get(i).toString());
                        }
                        fileHandler.writeThird(thirdQueries);
                        break;
                    }
                    case FOURTH: {
                        List<FourthQuery> fourthQueries = queryService.executeFourthQuery(START,FINISH);
                        for(int i=0;i<4;i++){
                            System.out.println(fourthQueries.get(i).toString());
                        }
                        fileHandler.writeFourth(fourthQueries);
                        break;
                    }
                    case FIFTH: {
                        List<FifthQuery> fifthQueries = queryService.executeFifthQuery(START,FINISH);
                        for(int i=0;i<20;i++){
                            System.out.println(fifthQueries.get(i).toString());
                        }
                        fileHandler.writeFifth(fifthQueries);
                        break;
                    }
                    case SIXTH: {
                        List<SixthQuery> sixthQueries = queryService.executeSixthQuery(START,FINISH);
                        for(int i=0;i<20;i++){
                            System.out.println(sixthQueries.get(i).toString());
                        }
                        fileHandler.writeSixth(sixthQueries);
                        break;
                    }
                }
                break;
            }
            case RANGE:{
                switcher.uploadByDateRange(START, FINISH);
                break;
            }
        }
    }


}
