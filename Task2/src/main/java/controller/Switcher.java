package controller;

import logic.data.CrimeDataHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.CrimeService;
import service.QueryService;
import service.StopService;
import util.DateHandler;

import java.util.List;

@Component
@Slf4j
public class Switcher {

    private static final String QUERY = "query";
    private static final String RANGE = "range";

    private CrimeDataHandler crimeDataHandler;
    private CrimeService crimeService;
    private DateHandler dateHandler;
    private StopService stopService;

    @Autowired
    public Switcher(CrimeDataHandler crimeDataHandler, CrimeService crimeService, DateHandler dateHandler, StopService stopService) {
        this.crimeDataHandler = crimeDataHandler;
        this.crimeService = crimeService;
        this.dateHandler = dateHandler;
        this.stopService = stopService;
    }

    public String getDecision(String start, String end, String query) {
        if ( query != null) {
            return QUERY;
        }
        return RANGE;
    }

    public void uploadByDateRange(String start, String end) {
        log.info("Search crimes by date range - request is being processed");
        List<String> dates = dateHandler.getDateRange(start, end);
        if (dates != null) {
            for (String date : dates) {
                uploadByDate(date, crimeService, crimeDataHandler);
            }
        }
        log.info("Your request successfully handled ");
    }

    public void uploadByDate(String date, CrimeService crimeService, CrimeDataHandler crimeDataHandler) {
        log.info("Search crimes by date - request is being processed");
        stopService.uploadByDateAndForce(date);
        stopService.uploadByDateAndLocation(date);
        crimeService.uploadCrimeByDataAndLocation(date);
        log.info("all crimes for the " + date + " was successfully uploaded ");
    }
}
