package util;

import com.opencsv.CSVReader;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import model.dto.CrimeDto;
import model.entity.Crime;
import model.entity.query.*;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class FileHandler {
    private static final String READ_PATH = "data/LondonStations.csv";
    private static final String WRITE_PATH = "data/crime.txt";
    private static final String QUERY_1_PATH = "data/1-QUERY.txt";
    private static final String QUERY_2_PATH = "data/2-QUERY.txt";
    private static final String QUERY_3_PATH = "data/3-QUERY.txt";
    private static final String QUERY_4_PATH = "data/4-QUERY.txt";
    private static final String QUERY_5_PATH = "data/5-QUERY.txt";
    private static final String QUERY_6_PATH = "data/6-QUERY.txt";

    public List<String[]> getLocations() {
        Path path = Paths.get(READ_PATH);
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(path);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return oneByOne(reader);
    }

    public List<String[]> getLocations(String userPath) {
        Path path = Paths.get(userPath);
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(path);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return oneByOne(reader);
    }

    private List<String[]> oneByOne(Reader reader) {
        List<String[]> list = new ArrayList<>();
        try {
            CSVReader csvReader = new CSVReader(reader);
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                list.add(line);
            }
            reader.close();

            csvReader.close();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return list;
    }

    public void writeFirst(List<FirstQuery> firstQueries){
        writeDataToFile(QUERY_1_PATH,firstQueries);
    }
    public void writeSecond(List<SecondQuery> secondQueries){
        writeDataToFile(QUERY_2_PATH,secondQueries);
    }
    public void writeThird(List<ThirdQuery> thirdQueries){
        writeDataToFile(QUERY_3_PATH,thirdQueries);
    }
    public void writeFourth(List<FourthQuery> fourthQueries){
        writeDataToFile(QUERY_4_PATH,fourthQueries);
    }
    public void writeFifth(List<FifthQuery> fifthQueries){
        writeDataToFile(QUERY_5_PATH,fifthQueries);
    }
    public void writeSixth(List<SixthQuery> sixthQueries){
        writeDataToFile(QUERY_6_PATH,sixthQueries);
    }

    public void writeDataToFile(String path , List<? extends Query> queries) {
        if (checkFile()) {
            try (FileWriter writer = new FileWriter(path, true)) {
                for (Query query : queries) {
                    writer.write(queries.toString());
                    writer.append('\n');
                }
                writer.flush();
            } catch (IOException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    public void writeDataToFile( List<CrimeDto> crimes) {
        if (checkFile()) {
            try (FileWriter writer = new FileWriter(WRITE_PATH, true)) {
                for (CrimeDto crime : crimes) {
                    writer.write(crime.toString());
                    writer.append('\n');
                }
                writer.flush();
            } catch (IOException ex) {
                log.error(ex.getMessage());
            }
        }
    }


    private boolean checkFile() {
        File file = new File(WRITE_PATH);
        if (file.exists()) {
            return true;
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
            return true;
        }
    }
}
