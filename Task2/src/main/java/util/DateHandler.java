package util;

import exception.DataConvertException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class DateHandler {

    private DataValidator validator;

    @Autowired
    public DateHandler(DataValidator validator) {
        this.validator = validator;
    }

    public List<String> getDateRange(String start, String end) {
        try {
            if (validator.checkDate(start)
                    && validator.checkDate(end)
                    && validator.checkPositiveRange(start, end)) {
                YearMonth startDate = validator.secureTransferToYearMonth(start);
                YearMonth endDate = validator.secureTransferToYearMonth(end);
                List<String> dates = new ArrayList<>();
                while (startDate.isBefore(endDate)) {
                    dates.add(startDate.toString());
                    startDate = startDate.plusMonths(1);
                }
                return dates;
            } else {
                log.error("Incorrect date format or your input is not positive range.");
            }
        } catch (DataConvertException e) {
            log.error(e.getMessage());
            return null;
        }
        return null;
    }

    public Date convertToDate(String datetime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = sdf.parse(datetime);
        return date;
    }
}
