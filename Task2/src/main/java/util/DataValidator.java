package util;

import exception.DataConvertException;
import org.springframework.stereotype.Component;

import java.time.YearMonth;
import java.time.format.DateTimeParseException;

@Component
public class DataValidator {

    public boolean checkDate(String date) {
        try {
            YearMonth someDate = YearMonth.parse(date);
            return true;
        }catch (DateTimeParseException e){
            return false;
        }
    }

    public boolean checkCoordinate(String lat) {
        try {
            Double someDate = Double.valueOf(lat);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    public boolean checkPositiveRange(String start, String end) {
        YearMonth startMonth = YearMonth.parse(start);
        YearMonth endMonth = YearMonth.parse(end);
        if (startMonth.isBefore(endMonth)){
            return true;
        }
        return false;
    }

    public Double secureTransferToDouble(String value) throws DataConvertException {
        try {
            Double someData = Double.valueOf(value);
            return someData;
        }catch (NumberFormatException e ){
            throw new DataConvertException("Your double value: "+value+" is not correct"+
                    e.getMessage());
        }
    }

    public YearMonth secureTransferToYearMonth(String date) throws DataConvertException {
        try {
            YearMonth someDate = YearMonth.parse(date);
            return someDate;
        }catch (DateTimeParseException e){
            throw new DataConvertException("Your date value: "+date+" is not correct"+
                    e.getMessage());
        }
    }
}
