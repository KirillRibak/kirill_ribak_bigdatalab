package exception;

public class DataConvertException extends Exception {
    public DataConvertException(String message) {
        super(message);
    }
}
