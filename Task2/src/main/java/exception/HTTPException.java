package exception;

public class
HTTPException extends Exception {

    public HTTPException(String message) {
        super(message);
    }

}
