package model.dto;

import lombok.Data;
import model.entity.OutcomeStatus;
import org.springframework.stereotype.Component;

import java.time.YearMonth;

@Data
@Component
public class CrimeDto implements Dto {
    private String category;
    private String locationType;
    private LocationDto location;
    private String context;
    private OutcomeStatus outcomeStatus;
    private String persistentId;
    private int id;
    private YearMonth month;
    private String locationSubtype;

    public CrimeDto() {
    }

    public CrimeDto(String category, String locationType, LocationDto location, String context, OutcomeStatus outcomeStatus, String persistentId, int id, YearMonth month, String locationSubtype) {
        this.category = category;
        this.locationType = locationType;
        this.location = location;
        this.context = context;
        this.outcomeStatus = outcomeStatus;
        this.persistentId = persistentId;
        this.id = id;
        this.month = month;
        this.locationSubtype = locationSubtype;
    }
}
