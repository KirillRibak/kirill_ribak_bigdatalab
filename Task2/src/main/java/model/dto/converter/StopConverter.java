package model.dto.converter;

import config.AppContext;
import model.dto.StopDto;
import model.entity.Stop;
import org.springframework.stereotype.Component;

@Component
public class StopConverter extends Converter<Stop, StopDto> {

    @Override
    public Stop convertToDbModel(StopDto stopDto) {
        Stop stop = AppContext.context.getBean(Stop.class);
        stop.setAgeRange(stopDto.getAgeRange());
        stop.setDatetime(stopDto.getDatetime());
        stop.setGender(stopDto.getGender());
        stop.setInvolvedPerson(stopDto.getInvolvedPerson());
        stop.setLegislation(stopDto.getLegislation());
        stop.setObjOfSearch(stopDto.getObjOfSearch());
        stop.setOfficerSkin(stopDto.getOfficerSkin());
        stop.setOperation(stopDto.getOperation());
        stop.setOperationName(stopDto.getOperationName());
        stop.setOutcome(stopDto.getOutcome());
        stop.setOutcomeLinkedToObjectOfSearch(stopDto.getOutcomeLinkedToObjectOfSearch());
        stop.setOutcomeObject(stopDto.getOutcomeObject().getId());
        stop.setRemovalOfMoreThanOuterClothing(stopDto.getRemovalOfMoreThanOuterClothing());
        stop.setSelfDefinedEthnicity(stopDto.getSelfDefinedEthnicity());
        stop.setType(stopDto.getType());
        return stop;
    }
}
