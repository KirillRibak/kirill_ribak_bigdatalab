package model.dto.converter;

public abstract class  Converter<DbEntity,Dto> {
    public abstract DbEntity convertToDbModel(Dto dto);
}
