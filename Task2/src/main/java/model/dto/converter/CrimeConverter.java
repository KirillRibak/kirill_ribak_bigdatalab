package model.dto.converter;

import config.AppContext;
import model.dto.CrimeDto;
import model.entity.Crime;

public class CrimeConverter extends Converter<Crime, CrimeDto>{

    @Override
    public Crime convertToDbModel(CrimeDto crimeDto) {
        Crime crime= AppContext.context.getBean(Crime.class);
        crime.setMonth(crimeDto.getMonth().toString());
        crime.setLocationSubtype(crimeDto.getLocationSubtype());
        crime.setPersistentId(crimeDto.getPersistentId());
        crime.setContext(crimeDto.getContext());
        crime.setCrimeCategory(crimeDto.getCategory());
        crime.setId(crimeDto.getId());
        crime.setLocationType(crimeDto.getLocationType());
        return crime;
    }
}
