package model.dto;

import config.AppContext;
import model.dto.converter.Converter;
import model.entity.Location;

public class LocationConverter extends Converter<Location,LocationDto> {
    @Override
    public Location convertToDbModel(LocationDto locationDto) {
        Location location = AppContext.context.getBean(Location.class);
        location.setLatitude(locationDto.getLatitude());
        location.setLongitude(locationDto.getLongitude());
        return location;
    }
}
