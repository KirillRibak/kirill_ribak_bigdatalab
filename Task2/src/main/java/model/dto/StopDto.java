package model.dto;

import lombok.Data;
import model.entity.OutcomeObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@Component
public class StopDto implements Dto {
    private String ageRange;
    private String outcome;
    private Boolean involvedPerson;
    private String selfDefinedEthnicity;
    private String gender;
    private String legislation;
    private Boolean outcomeLinkedToObjectOfSearch;
    private Date datetime;
    private Boolean removalOfMoreThanOuterClothing;
    private OutcomeObject outcomeObject;
    private LocationDto locationDto;
    private Boolean operation;
    private String officerSkin;
    private String type;
    private String operationName;
    private String objOfSearch;
}
