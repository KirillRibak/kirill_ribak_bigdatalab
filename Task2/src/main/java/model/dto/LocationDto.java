package model.dto;

import lombok.Data;
import model.entity.Street;
import org.springframework.stereotype.Component;

@Data
@Component
public class LocationDto implements Dto {
    private Double latitude;
    private Double longitude;
    private Street street;

    public LocationDto(){}

    public LocationDto(Double latitude, Double longitude, Street street) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.street = street;
    }


}
