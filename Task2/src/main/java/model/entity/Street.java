package model.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Street {
    private Integer id;
    private String name;

    public Street(){}

    public Street(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
