package model.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@Component
public class Stop extends DbEntity {
    private String ageRange;
    private String outcome;
    private Boolean involvedPerson;
    private String selfDefinedEthnicity;
    private String gender;
    private String legislation;
    private Boolean outcomeLinkedToObjectOfSearch;
    private Date datetime;
    private Boolean removalOfMoreThanOuterClothing;
    private String outcomeObject;
    private Integer locationId;
    private Boolean operation;
    private String officerSkin;
    private String type;
    private String operationName;
    private String objOfSearch;
}
