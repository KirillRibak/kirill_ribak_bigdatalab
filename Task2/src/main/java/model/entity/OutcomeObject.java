package model.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class OutcomeObject extends DbEntity {
    private String id;
    private String name;
}
