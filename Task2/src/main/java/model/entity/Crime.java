package model.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Crime extends DbEntity{
    private String locationType;
    private String crimeCategory;
    private int locationId;
    private String context;
    private Integer outcomeStatusId;
    private String persistentId;
    private Integer id;
    private String month;
    private String locationSubtype;

    public Crime() {
    }

    public Crime(String locationType, String crimeCategory, int locationId, String context,
                 int outcomeStatusId, String persistentId, int id, String month, String locationSubtype) {
        this.locationType = locationType;
        this.crimeCategory = crimeCategory;
        this.locationId = locationId;
        this.context = context;
        this.outcomeStatusId = outcomeStatusId;
        this.persistentId = persistentId;
        this.id = id;
        this.month = month;
        this.locationSubtype = locationSubtype;
    }
}
