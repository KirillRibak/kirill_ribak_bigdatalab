package model.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public abstract class DbEntity {
    protected int dbId;
}
