package model.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.YearMonth;

@Data
@Component
public class OutcomeStatus extends DbEntity{

    private String category;
    private YearMonth date;

    public OutcomeStatus(){}

    public OutcomeStatus(String category, YearMonth date) {
        this.category = category;
        this.date = date;
    }
}
