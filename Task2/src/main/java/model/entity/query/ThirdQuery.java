package model.entity.query;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class ThirdQuery extends Query {
    private int streetId;
    private String streetName;
    private String outcomeCategory;
    private int countCrimes;
    private String percent;

    @Override
    public String toString() {
        return "------------------------------------------------------------------------------" +
                "---------------------------------------------------------------------------------------------------\n" +
                "|streetId=" + streetId + 
                "|streetName=" + streetName + 
                "|outcomeCategory=" + outcomeCategory +
                "|countCrimes=" + countCrimes + 
                "|percent=" + percent + "|\n" +
                "--------------------------------------------------------------------------------------------------" +
                "-------------------------------------------------------------------------------" ;
    }
}
