package model.entity.query;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class SixthQuery  extends Query{
    private int id;
    private String name;
    private String month;
    private int crimeDrug;
    private int stopDrug;
    private int crimeWeapon;
    private int stopWeapon;
    private int crimeTheft;
    private int stopTheft;

    @Override
    public String toString() {
        return "-----------------------------------------------------------------------------------" +
                "----------------------------------------------------------------------------------------------\n" +
                "|id=" + id +
                "|name=" + name +
                "|month=" + month +
                "|crimeDrug=" + crimeDrug +
                "|stopDrug=" + stopDrug +
                "|crimeWeapon=" + crimeWeapon +
                "|stopWeapon=" + stopWeapon +
                "|crimeTheft=" + crimeTheft +
                "|stopTheft=" + stopTheft + "|\n" +
                "---------------------------------------------------------------------" +
                "------------------------------------------------------------------------------------------------------------" ;
    }
}
