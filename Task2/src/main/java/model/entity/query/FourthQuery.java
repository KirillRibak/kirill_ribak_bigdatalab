package model.entity.query;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class FourthQuery  extends Query{
    private String officerEthnicity;
    private int stopAndSearchCount;
    private String arrestStatistic;
    private String noActionStatistic;
    private String otherStatistic;
    private String mostCommonObjectOfSearch;

    @Override
    public String toString() {
        return  "-------------------------------------------------------------------------------" +
                "--------------------------------------------------------------------------------------------------\n" +
                "|officerEthnicity=" + officerEthnicity + "|"+
                "stopAndSearchCount=" + stopAndSearchCount +"|"+
                "arrestStatistic=" + arrestStatistic + "|"+
                "noActionStatistic=" + noActionStatistic + "|"+
                "otherStatistic=" + otherStatistic +"|"+
                "mostCommonObjectOfSearch=" + mostCommonObjectOfSearch +"|\n"+
                "----------------------------------------------------------------------" +
                "-----------------------------------------------------------------------------------------------------------" ;
    }
}
