package model.entity.query;

public  abstract class Query {
    public static final String FIRST_QUERY = "SELECT\n" +
            "streets.id AS street_id ,\n" +
            "streets.name AS street_name ,\n" +
            "FORMAT('from %s till %s',MIN(crime.month),MAX(crime.month)) AS \"period\",\n" +
            "COUNT(crime.id) AS crime_count\n" +
            "FROM public.crime_street crime\n" +
            "INNER JOIN location locations ON crime.location_id=locations.id\n" +
            "INNER JOIN street streets ON locations.street_id = streets.id\n" +
            "WHERE crime.month >= ? AND crime.month <= ?\n" +
            "GROUP BY (streets.id)\n" +
            "ORDER BY  (crime_count) DESC ";

    public static final String SECOND_QUERY = "WITH crimes_by_category_and_month AS(\n" +
            "SELECT crime_street.crime_category AS category, crime_street.month AS month, \n" +
            "count(crime_street.id)AS crimes\n" +
            "FROM crime_street WHERE crime_street.month >= ? AND crime_street.month <= ?\n" +
            "GROUP BY (crime_street.crime_category, crime_street.month) ORDER BY category,month\n" +
            ")\n" +
            "SELECT category,month,crimes,\n" +
            "LAG(crimes ,1)OVER(PARTITION BY category) AS previous_month_crime ,\n" +
            "crimes - LAG(crimes ,1)OVER(PARTITION BY category) AS delta_count,\n" +
            "FORMAT(' %s %%', TRUNC((crimes - LAG(crimes ,1)OVER(PARTITION BY category))*100/LAG(crimes ,1)OVER(PARTITION BY category), 2)) AS basic_growth_rate\n" +
            "FROM crimes_by_category_and_month";

    public static final String THIRD_QUERY = "SELECT streets.id AS street_id,\n" +
            "streets.name AS street_name,\n" +
            "outcome.category AS outcome_category,\n" +
            "COUNT(outcome.category) AS crimes,\n" +
            "FORMAT(' %s %%', TRUNC(COUNT(outcome.category)*100/SUM(COUNT(outcome.category))OVER(),3)) AS percent\n" +
            "FROM crime_street\n" +
            "INNER JOIN outcome_status outcome\n" +
            "ON  outcome.id=crime_street.outcome_status_id\n" +
            "INNER JOIN location locations\n" +
            "ON crime_street.location_id=locations.id\n" +
            "INNER JOIN street streets\n" +
            "ON locations.street_id = streets.id\n" +
            "WHERE crime_street.month >= ?\n" +
            "AND crime_street.month <= ?\n" +
            "AND crime_street.outcome_status_id IS NOT NULL\n" +
            "AND outcome.category= ?\n" +
            "GROUP BY(streets.id,outcome.category)\n" +
            "ORDER BY (crimes) DESC";

    public static final String FOURTH_QUERY = "  WITH stop_and_search_by_officer_ethnicity AS (\n" +
            " \tSELECT stop.officer_defined_ethnicity AS officer_ethnicity,\n" +
            " \tCOUNT(stop.id) AS stops\n" +
            " \tFROM stop\n" +
            " \tWHERE\n" +
            " \tto_char(stop.datetime, 'YYYY-MM')>=?\n" +
            " \tAND to_char(stop.datetime, 'YYYY-MM')<=?\n" +
            " \tGROUP BY (officer_ethnicity)\n" +
            " ),\n" +
            " arrest_statistic AS (\n" +
            "\t \tSELECT stop.officer_defined_ethnicity AS officer_ethnicity,\n" +
            " \tCOUNT(stop.id) AS stops,\n" +
            "\tFORMAT(' %s %%', TRUNC(sum(COUNT(stop.id))over(partition by stop.officer_defined_ethnicity)/sum(COUNT(stop.id))over(), 3))\n" +
            "\tAS arrest_statistic\n" +
            " \tFROM stop\n" +
            " \tWHERE\n" +
            " \tto_char(stop.datetime, 'YYYY-MM')>=?\n" +
            " \tAND to_char(stop.datetime, 'YYYY-MM')<=?\n" +
            "\tand outcome='Arrest'\n" +
            " \tGROUP BY (officer_ethnicity)\n" +
            " ),\n" +
            " no_action AS (\n" +
            " \tSELECT stop.officer_defined_ethnicity AS officer_ethnicity,\n" +
            " \tCOUNT(stop.id) AS stops,\n" +
            "\tFORMAT(' %s %%', TRUNC(sum(COUNT(stop.id))over(partition by stop.officer_defined_ethnicity)/sum(COUNT(stop.id))over(), 3))\n" +
            "\tAS no_action\n" +
            " \tFROM stop\n" +
            " \tWHERE\n" +
            " \tto_char(stop.datetime, 'YYYY-MM')>=?\n" +
            " \tAND to_char(stop.datetime, 'YYYY-MM')<=?\n" +
            "\tand outcome='A no further action disposal'\n" +
            " \tGROUP BY (officer_ethnicity)\n" +
            " ),\n" +
            " other AS(\n" +
            " \tSELECT stop.officer_defined_ethnicity AS officer_ethnicity,\n" +
            " \tCOUNT(stop.id) AS stops,\n" +
            "\tFORMAT(' %s %%', TRUNC(sum(COUNT(stop.id))over(partition by stop.officer_defined_ethnicity)/sum(COUNT(stop.id))over(), 3))\n" +
            "\tAS other\n" +
            " \tFROM stop\n" +
            " \tWHERE\n" +
            " \tto_char(stop.datetime, 'YYYY-MM')>=?\n" +
            " \tAND to_char(stop.datetime, 'YYYY-MM')<=?\n" +
            "\tAND outcome!='A no further action disposal'\n" +
            "\tAND outcome!='Arrest'\n" +
            " \tGROUP BY (officer_ethnicity)\n" +
            " ),\n" +
            " common AS(\n" +
            "\tSELECT officer_defined_ethnicity AS eth,object_of_search,\n" +
            "    COUNT(object_of_search) AS value\n" +
            "    FROM stop\n" +
            "    GROUP BY eth,object_of_search,officer_defined_ethnicity\n" +
            "    ORDER BY eth , value DESC)\n" +
            "SELECT DISTINCT stop_and_search_by_officer_ethnicity.officer_ethnicity ,\n" +
            "stop_and_search_by_officer_ethnicity.stops,\n" +
            "arrest.arrest_statistic,\n" +
            "without_action.no_action\n" +
            "AS no_action_statistic,\n" +
            "othr.other\n" +
            "AS other_statistic,\n" +
            "FIRST_VALUE(object_of_search)OVER(PARTITION BY stop_and_search_by_officer_ethnicity.officer_ethnicity)\n" +
            "AS common_object\n" +
            "FROM stop_and_search_by_officer_ethnicity\n" +
            "INNER JOIN arrest_statistic arrest\n" +
            "ON stop_and_search_by_officer_ethnicity.officer_ethnicity = arrest.officer_ethnicity\n" +
            "INNER JOIN no_action without_action\n" +
            "ON stop_and_search_by_officer_ethnicity.officer_ethnicity = without_action.officer_ethnicity\n" +
            "INNER JOIN other othr\n" +
            "ON stop_and_search_by_officer_ethnicity.officer_ethnicity = othr.officer_ethnicity\n" +
            "INNER JOIN common objects\n" +
            "ON stop_and_search_by_officer_ethnicity.officer_ethnicity = objects.eth";

    public static final String FIFTH_QUERY = "WITH \"all\" AS (\n" +
            "    SELECT location_id,\n" +
            "           age_range,\n" +
            "           sum(count(*)) OVER (PARTITION BY location_id, age_range) AS \"age_count\",\n" +
            "           gender,\n" +
            "           sum(count(*)) OVER (PARTITION BY location_id, gender) AS \"gender_count\",\n" +
            "           officer_defined_ethnicity,\n" +
            "           sum(count(*)) OVER (PARTITION BY location_id, officer_defined_ethnicity) AS \"ethnicity_count\",\n" +
            "           object_of_search,\n" +
            "           sum(count(*)) OVER (PARTITION BY location_id, object_of_search) AS \"object_count\",\n" +
            "           outcome,\n" +
            "           sum(count(*)) OVER (PARTITION BY location_id, outcome) AS \"outcome_count\"\n" +
            "    FROM stop\n" +
            "    WHERE age_range<>''\n" +
            "      AND officer_defined_ethnicity<>''\n" +
            "      AND object_of_search<>''\n" +
            "      AND outcome<>''\n" +
            "      AND location_id IS NOT NULL\n" +
            "      AND to_char(datetime,'YYYY-MM') BETWEEN ? AND ?\n" +
            "    GROUP BY location_id, age_range, gender, officer_defined_ethnicity, object_of_search, outcome\n" +
            ")\n" +
            "SELECT DISTINCT location_id,\n" +
            "                st.name,\n" +
            "                first_value(age_range) OVER (PARTITION BY location_id ORDER BY age_count DESC) AS popular_age,\n" +
            "                first_value(gender) OVER (PARTITION BY location_id ORDER BY gender_count DESC) AS popular_gender,\n" +
            "                first_value(officer_defined_ethnicity) OVER (PARTITION BY location_id ORDER BY ethnicity_count DESC) AS popular_ethnicity,\n" +
            "                first_value(object_of_search) OVER (PARTITION BY location_id ORDER BY object_count DESC) AS popular_object_of_search,\n" +
            "                first_value(outcome) OVER (PARTITION BY location_id ORDER BY outcome_count DESC) AS popular_outcome\n" +
            "FROM \"all\"\n" +
            "INNER JOIN street st ON location_id = st.id\n" +
            "GROUP BY location_id, st.name, age_range, outcome, object_of_search, officer_defined_ethnicity, gender, age_count, gender_count, ethnicity_count, object_count, outcome_count;";

    public static final String SIXTH_QUERY = "  WITH crimes AS (\n" +
            "        SELECT location_id,\n" +
            "               crime_category,\n" +
            "               month\n" +
            "        FROM crime_street\n" +
            "        WHERE (crime_category = 'drugs'\n" +
            "            OR crime_category = 'possession-of-weapons'\n" +
            "            OR crime_category = 'theft-from-the-person'\n" +
            "            OR crime_category = 'shoplifting')\n" +
            "          AND (month between ? AND ?)\n" +
            "          AND location_id IS NOT NULL\n" +
            "\n" +
            "    ), stop_search AS (\n" +
            "        SELECT location_id,\n" +
            "               object_of_search,\n" +
            "               to_char(datetime, 'yyyy-MM') AS \"date\"\n" +
            "        FROM stop\n" +
            "        WHERE (object_of_search = 'Controlled drugs'\n" +
            "            OR object_of_search = 'Firearms'\n" +
            "            OR object_of_search = 'Offensive weapons'\n" +
            "            OR object_of_search = 'Stolen goods')\n" +
            "          AND (to_char(datetime, 'yyyy-MM') BETWEEN ? and ?)\n" +
            "          AND outcome = 'Arrest'\n" +
            "          AND location_id is not null\n" +
            "\n" +
            "    ), data AS (\n" +
            "        SELECT DISTINCT location_id,\n" +
            "                        month\n" +
            "        FROM crimes\n" +
            "        UNION\n" +
            "        DISTINCT\n" +
            "        SELECT location_id,\n" +
            "               \"date\"\n" +
            "        FROM stop_search\n" +
            "    )\n" +
            "    SELECT street.id,\n" +
            "           street.name,\n" +
            "           d.month,\n" +
            "           CRDrugs.drugs AS drug_crime_count,\n" +
            "           SSDrugs.\"controlled drugs\" AS drug_stop_count,\n" +
            "           CRWeapons.\"possession of weapons\" AS weapon_crime_count,\n" +
            "           SSWeapons.\"Offensive weapons/Firearms\" AS weapon_stop_count,\n" +
            "           CRTheft.\"theft from the person/shoplifting\" AS theft_crime_count ,\n" +
            "           SSTheft.\"Stolen goods\" AS theft_stop_count\n" +
            "    FROM \"data\" d\n" +
            "             LEFT JOIN (\n" +
            "        SELECT location_id,\n" +
            "               month,\n" +
            "               COUNT(*) AS \"drugs\"\n" +
            "        FROM crimes\n" +
            "        WHERE crime_category='drugs'\n" +
            "        GROUP BY location_id, month\n" +
            "    ) AS CRDrugs ON d.location_id=CRDrugs.location_id AND d.month = CRDrugs.month\n" +
            "             LEFT JOIN (\n" +
            "        SELECT location_id,\n" +
            "               \"date\",\n" +
            "               COUNT(*) AS \"controlled drugs\"\n" +
            "        FROM stop_search\n" +
            "        WHERE object_of_search='Controlled drugs'\n" +
            "        GROUP BY location_id, \"date\"\n" +
            "    ) SSDrugs ON d.location_id = SSDrugs.location_id AND d.month = SSDrugs.date\n" +
            "             LEFT JOIN (\n" +
            "        SELECT location_id,\n" +
            "               month,\n" +
            "               COUNT(*) AS \"possession of weapons\"\n" +
            "        FROM crimes\n" +
            "        WHERE crime_category='possession-of-weapons'\n" +
            "        GROUP BY location_id, month\n" +
            "    ) CRWeapons ON d.location_id = CRWeapons.location_id AND d.month = CRWeapons.month\n" +
            "             LEFT JOIN (\n" +
            "        SELECT location_id,\n" +
            "               \"date\",\n" +
            "               COUNT(*) AS \"Offensive weapons/Firearms\"\n" +
            "        FROM stop_search\n" +
            "        WHERE object_of_search='Offensive weapons'\n" +
            "           OR object_of_search='Firearms'\n" +
            "        GROUP BY location_id, \"date\"\n" +
            "    ) SSWeapons ON d.location_id = SSWeapons.location_id AND d.month = SSWeapons.date\n" +
            "             LEFT JOIN (\n" +
            "        SELECT location_id,\n" +
            "               month,\n" +
            "               COUNT(*) AS \"theft from the person/shoplifting\"\n" +
            "        FROM crimes\n" +
            "        WHERE crime_category='theft-from-the-person'\n" +
            "           OR crime_category='shoplifting'\n" +
            "        GROUP BY location_id, month\n" +
            "    ) CRTheft ON d.location_id = CRTheft.location_id AND d.month = CRTheft.month\n" +
            "             LEFT JOIN (\n" +
            "        SELECT location_id,\n" +
            "               \"date\",\n" +
            "               COUNT(*) AS \"Stolen goods\"\n" +
            "        FROM stop_search\n" +
            "        WHERE object_of_search='Stolen goods'\n" +
            "        GROUP BY location_id, \"date\"\n" +
            "    ) SSTheft ON d.location_id = SSTheft.location_id AND d.month = SSTheft.date\n" +
            "    INNER JOIN location ON location.id = d.location_id\n" +
            "    INNER JOIN street ON street.id=location.street_id";
}
