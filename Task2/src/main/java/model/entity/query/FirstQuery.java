package model.entity.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Data
@Component
public class FirstQuery  extends Query{
    private int streetId;
    private String streetName;
    private String period;
    private int crimeCount;

    @Override
    public String toString() {
        return  "---------------------------------------------------------------------" +
                "------------------------------------------------------------------------------------------------------------\n" +
                "|streetId=" + streetId + "|"+
                "streetName=" + streetName + "|"+
                "period=" + period + "|"+
                "crimeCount=" + crimeCount+"|\n"+
                "---------------------------------------------------------------------------------" +
                "------------------------------------------------------------------------------------------------" ;
    }
}
