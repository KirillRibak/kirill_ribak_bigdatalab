package model.entity.query;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.YearMonth;

@Component
@Data
public class SecondQuery extends Query {
    private String category;
    private YearMonth month;
    private int crimes;
    private int previousMonthCrimes;
    private int deltaCount;
    private String percent;

    @Override
    public String toString() {
        return  "---------------------------------------------------------------------------" +
                "------------------------------------------------------------------------------------------------------\n" +
                "|category=" + category +
                "|month=" + month +
                "|crimes=" + crimes +
                "|previousMonthCrimes=" + previousMonthCrimes +
                "|deltaCount=" + deltaCount +
                "|percent=" + percent + "|\n" +
                "-----------------------------------------------------------------" +
                "----------------------------------------------------------------------------------------------------------------" ;
    }
}
