package model.entity.query;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class FifthQuery  extends Query{
    private String ageRange;
    private String gender;
    private String ethnicity;
    private String objectOfSearch;
    private String outcome;

    @Override
    public String toString() {
        return  "--------------------------------------------------------------------------------------" +
                "-------------------------------------------------------------------------------------------\n" +
                "|ageRange=" + ageRange + "|"+
                "gender=" + gender + "|"+
                "ethnicity=" + ethnicity + "|"+
                "objectOfSearch=" + objectOfSearch + "|" +
                "outcome=" + outcome + "|\n" +
                "-----------------------------------------------------------------" +
                "----------------------------------------------------------------------------------------------------------------" ;
    }
}
