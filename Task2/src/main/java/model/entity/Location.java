package model.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Location extends DbEntity{
    private Double latitude;
    private Double longitude;
    private int streetId;

    public Location(){}

    public Location(Double latitude, Double longitude, int streetId) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.streetId = streetId;
    }
}
