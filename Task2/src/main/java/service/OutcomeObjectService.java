package service;

import model.entity.OutcomeObject;

public interface OutcomeObjectService {
    void uploadOutcomeObjectToDb(OutcomeObject outcomeObject);
}
