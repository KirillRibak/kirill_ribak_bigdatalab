package service;

import model.entity.Street;

public interface StreetService {
    void uploadStreet(Street street);
}
