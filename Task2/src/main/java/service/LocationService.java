package service;

import model.dto.LocationDto;
import model.entity.Location;

import java.util.List;

public interface LocationService {
    List<Location> getLocation();

    int linkLocationWithStreet(Integer streetId, LocationDto location);

    int findLocation(Double lat, Double lng);
}
