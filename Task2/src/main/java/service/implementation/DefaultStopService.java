package service.implementation;

import dao.StopDao;
import logic.data.ForceDataHandler;
import logic.data.StopDataHandler;
import lombok.extern.slf4j.Slf4j;
import model.dto.LocationDto;
import model.dto.StopDto;
import model.dto.converter.StopConverter;
import model.entity.Location;
import model.entity.Stop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.LocationService;
import service.OutcomeObjectService;
import service.StopService;
import service.StreetService;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DefaultStopService implements StopService {

    private StopDao stopDao;
    private StopConverter stopConverter;
    private LocationService locationService;
    private StreetService streetService;
    private OutcomeObjectService outcomeObjectService;
    private StopDataHandler stopDataHandler;
    private ForceDataHandler forceDataHandler;

    public DefaultStopService(StopDao stopDao, StopConverter stopConverter, LocationService locationService,
                              StreetService streetService, OutcomeObjectService outcomeObjectService,
                              StopDataHandler stopDataHandler, ForceDataHandler forceDataHandler) {
        this.stopDao = stopDao;
        this.stopConverter = stopConverter;
        this.locationService = locationService;
        this.streetService = streetService;
        this.outcomeObjectService = outcomeObjectService;
        this.stopDataHandler = stopDataHandler;
        this.forceDataHandler = forceDataHandler;
    }

    @Override
    public void uploadStopToDb(List<StopDto> stopDtoList) {
        List<Stop>stops = getStops(stopDtoList);
        stops.parallelStream().forEach(stop ->
                stopDao.uploadStopToDb(stop)
        );
    }

    @Override
    public void uploadByDateAndLocation(String date) {
        List<Location> locations = locationService.getLocation();
        for (Location location: locations) {
            List<StopDto> stopsLocations = stopDataHandler.getStopsByDateAndLocation(date, location);
            uploadStopToDb(stopsLocations);
        }
    }

    @Override
    public void uploadByDateAndForce(String date) {
        List<String> forces = forceDataHandler.getAllForces();
        for(String force:forces){
            List<StopDto> stopDtos = stopDataHandler.getStopsByDateAndForce(date,force);
            uploadStopToDb(stopDtos);
        }
    }


    private List<Stop> getStops(List<StopDto> dto){
        List<Stop> stops = new ArrayList<>();
        for (StopDto stopDto : dto){
            Integer locationId = getLocationId(stopDto);
            Stop stop = stopConverter.convertToDbModel(stopDto);
            stop.setLocationId(locationId);
            stops.add(stop);
            outcomeObjectService.uploadOutcomeObjectToDb(stopDto.getOutcomeObject());
        }
        return stops;
    }

    private Integer getLocationId(StopDto stopDto){
        LocationDto location = stopDto.getLocationDto();
        if (location!=null) {
            streetService.uploadStreet(location.getStreet());
            Double lat = stopDto.getLocationDto().getLatitude();
            Double lng = stopDto.getLocationDto().getLongitude();
            Integer locationId = locationService.findLocation(lat, lng);
            Integer streetId = location.getStreet().getId();
            locationService.linkLocationWithStreet(streetId, location);
            return locationId;
        }
       return null;
    }
}
