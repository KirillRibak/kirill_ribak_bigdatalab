package service.implementation;

import config.AppContext;
import dao.LocationDao;
import model.dto.LocationConverter;
import model.dto.LocationDto;
import model.entity.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.LocationService;
import util.FileHandler;

import java.util.ArrayList;
import java.util.List;

@Component
public class DefaultLocationService implements LocationService {

    private FileHandler fileHandler;
    private LocationConverter locationConverter;
    private LocationDao locationDao;

    @Autowired
    public DefaultLocationService(FileHandler fileHandler, LocationConverter locationConverter, LocationDao locationDao) {
        this.fileHandler = fileHandler;
        this.locationConverter = locationConverter;
        this.locationDao = locationDao;
    }


    @Override
    public List<Location> getLocation() {
        List<String[]> locationsString = fileHandler.getLocations();
        return convert(locationsString);
    }

    @Override
    public int linkLocationWithStreet(Integer streetId, LocationDto location) {
        Location locationDb = locationConverter.convertToDbModel(location);
        int locationId = locationDao.
                uploadLocation(locationDb);
        locationDao.linkLocationWithStreet(locationId, streetId);
        return locationId;
    }

    @Override
    public int findLocation(Double lat, Double lng) {
        int id = locationDao.getLocationIdByLatitudeAndLongitude(lat,lng);
        return id;
    }

    private List<Location> convert(List<String[]> listCoordinates) {
        List<Location> locations = new ArrayList<>();
        for (int j = 1; j < listCoordinates.size(); j++) {
            String[] coordinates = listCoordinates.get(j);
            Location location = AppContext.context.getBean(Location.class);
            location.setLongitude(Double.valueOf(coordinates[1]));
            location.setLatitude(Double.valueOf(coordinates[2]));
            locations.add(location);
        }
        return locations;
    }
}
