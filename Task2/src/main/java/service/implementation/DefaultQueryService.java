package service.implementation;

import dao.QueryDao;
import model.entity.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.QueryService;
import util.DataValidator;

import java.util.List;

@Component
public class DefaultQueryService implements QueryService {

    private QueryDao queryDao;
    private DataValidator dataValidator;

    @Autowired
    public DefaultQueryService(QueryDao queryDao, DataValidator dataValidator) {
        this.queryDao = queryDao;
        this.dataValidator = dataValidator;
    }

    @Override
    public List<FirstQuery> executeFirstQuery(String startDate, String finishDate) {
        if (dataValidator.checkDate(startDate)
                && dataValidator.checkDate(finishDate)
                && dataValidator.checkPositiveRange(startDate, finishDate)) {
            return queryDao.executeFirstQuery(startDate, finishDate);
        }
        return null;
    }

    @Override
    public List<SecondQuery> executeSecondQuery(String startDate, String finishDate) {
        if (dataValidator.checkDate(startDate)
                && dataValidator.checkDate(finishDate)
                && dataValidator.checkPositiveRange(startDate, finishDate)) {
            return queryDao.executeSecondQuery(startDate, finishDate);
        }
        return null;
    }

    @Override
    public List<ThirdQuery> executeThirdQuery(String startDate, String finishDate,String category) {
        if (dataValidator.checkDate(startDate)
                && dataValidator.checkDate(finishDate)
                && dataValidator.checkPositiveRange(startDate, finishDate)) {
            return queryDao.executeThirdQuery(startDate, finishDate, category);
        }
        return null;
    }

    @Override
    public List<FourthQuery> executeFourthQuery(String startDate, String finishDate) {
        return queryDao.executeFourthQuery(startDate, finishDate);

    }

    @Override
    public List<FifthQuery> executeFifthQuery(String startDate, String finishDate) {
        return queryDao.executeFifthQuery(startDate, finishDate);
    }

    @Override
    public List<SixthQuery> executeSixthQuery(String startDate, String finishDate) {
        return queryDao.executeSixthQuery(startDate,finishDate);
    }
}
