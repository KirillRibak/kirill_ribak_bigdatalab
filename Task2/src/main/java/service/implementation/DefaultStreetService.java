package service.implementation;

import dao.StreetDao;
import lombok.extern.slf4j.Slf4j;
import model.entity.Street;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import service.StreetService;

@Component
@Slf4j
public class DefaultStreetService implements StreetService {

    private StreetDao streetDao;

    @Autowired
    public DefaultStreetService(StreetDao streetDao) {
        this.streetDao = streetDao;
    }

    @Override
    public void uploadStreet(Street street) {
        streetDao.uploadStreet(street);
    }
}
