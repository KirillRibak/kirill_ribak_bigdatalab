package service.implementation;

import dao.CrimeDao;
import dao.OutcomeStatusDao;
import logic.data.CrimeDataHandler;
import lombok.extern.slf4j.Slf4j;
import model.dto.converter.CrimeConverter;
import model.dto.CrimeDto;
import model.dto.LocationDto;
import model.entity.Crime;
import model.entity.Location;
import model.entity.Street;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.CrimeService;
import service.LocationService;
import service.StreetService;
import util.FileHandler;

import java.util.List;

@Component
@Slf4j
public class DefaultCrimeService implements CrimeService {

    private CrimeConverter crimeConverter;
    private CrimeDao crimeDao;
    private OutcomeStatusDao outcomeStatusDao;
    private FileHandler fileHandler;
    private StreetService streetService;
    private LocationService locationService;
    private CrimeDataHandler crimeDataHandler;

    @Autowired
    public DefaultCrimeService(CrimeConverter crimeConverter, CrimeDao crimeDao,
                               OutcomeStatusDao outcomeStatusDao,
                               FileHandler fileHandler, StreetService streetService,
                               LocationService locationService, CrimeDataHandler crimeDataHandler) {
        this.crimeConverter = crimeConverter;
        this.crimeDao = crimeDao;
        this.outcomeStatusDao = outcomeStatusDao;
        this.fileHandler = fileHandler;
        this.streetService = streetService;
        this.locationService = locationService;
        this.crimeDataHandler = crimeDataHandler;
    }

    @Override
    public void uploadCrimeToDb(List<CrimeDto> crimes) {
        crimes.parallelStream().forEach(crimeDto ->
                parseCrime(crimeDto, crimeConverter.convertToDbModel(crimeDto),
                        crimeDto.getLocation().getStreet(), crimeDto.getLocation())

        );
    }

    @Override
    public void uploadCrimeToFile(List<CrimeDto> crime) {
        fileHandler.writeDataToFile(crime);
    }

    @Override
    public void uploadCrimeByDataAndLocation(String date) {
        List<Location> locations = locationService.getLocation();
        for(Location location: locations){
            List<CrimeDto> crimeDtos = crimeDataHandler.getCrimeByDate(location,date);
            uploadCrimeToDb(crimeDtos);
        }
    }

    private void parseCrime(CrimeDto crime, Crime crimeDb, Street street, LocationDto location) {
        Integer streetId = street.getId();
        streetService.uploadStreet(street);
        int outcomeStatusId = getOutcomeStatusId(crime);
        uploadCrime(crimeDb, streetId, outcomeStatusId, location);
    }

    private int getOutcomeStatusId(CrimeDto crime) {
        if (crime.getOutcomeStatus() != null) {
            return outcomeStatusDao.uploadOutcomeStatus(crime.getOutcomeStatus());
        }
        return 0;
    }

    private void uploadCrime(Crime crimeDb, Integer streetId, int outcomeStatusId, LocationDto location) {
        int locationId = locationService.linkLocationWithStreet(streetId, location);
        crimeDb.setLocationId(locationId);
        log.info("Uploading new crime to Db.");
        if (outcomeStatusId != 0) {
            crimeDb.setOutcomeStatusId(outcomeStatusId);
        }
        crimeDao.uploadCrimeToDb(crimeDb);
    }

}