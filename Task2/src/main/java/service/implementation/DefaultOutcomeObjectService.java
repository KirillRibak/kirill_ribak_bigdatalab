package service.implementation;

import dao.OutcomeObjectDao;
import model.entity.OutcomeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.OutcomeObjectService;

@Component
public class DefaultOutcomeObjectService implements OutcomeObjectService {

    private OutcomeObjectDao outcomeObjectDao;

    @Autowired
    public DefaultOutcomeObjectService(OutcomeObjectDao outcomeObjectDao) {
        this.outcomeObjectDao = outcomeObjectDao;
    }

    @Override
    public void uploadOutcomeObjectToDb(OutcomeObject outcomeObject) {
        outcomeObjectDao.uploadOutcomeObjectToDb(outcomeObject);
    }
}
