package service;

import model.dto.CrimeDto;
import model.entity.Crime;
import model.entity.Location;

import java.util.List;

public interface CrimeService {
    void uploadCrimeToDb(List<CrimeDto> crime);

    void uploadCrimeToFile(List<CrimeDto> crime);

    void uploadCrimeByDataAndLocation(String date);
}
