package service;

import model.dto.StopDto;
import model.entity.Location;

import java.util.List;

public interface StopService {

    void uploadStopToDb(List<StopDto> stops);

    void uploadByDateAndLocation(String date);

    void uploadByDateAndForce(String date);
}
